<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="section mast">

      <div class="container">

        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

          <p>&nbsp;</p>
          <p>&nbsp;</p>

        <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

        <?php the_content(); ?>

        <?php endwhile; else: ?>

        <h2>Woops...</h2>

        <p>Sorry, no posts we're found.</p>

        <?php endif; ?>

        <p align="center"><?php posts_nav_link(); ?></p>

      </div>

    </section>

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
