(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {};

// library properties:
lib.properties = {
	width: 970,
	height: 800,
	fps: 24,
	color: "#EAEAEB",
	opacity: 1.00,
	webfonts: {},
	manifest: []
};



lib.ssMetadata = [];


lib.webfontAvailable = function(family) {
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.Tween7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EABE51").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA3AAAmAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");
	this.shape.setTransform(0,0,1.189,1.189);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.7,-15.7,31.4,31.4);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#EABE51").ss(5.5,0,1).p("AhkAAQAAgpAegdQAdgeApAAQAqAAAeAeQAdAdAAApQAAAqgdAdQgeAegqAAQgpAAgdgeQgegdAAgqg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.9,-12.9,25.9,25.9);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#EABE51").ss(5.5,0,1).p("AhkAAQAAgpAegdQAdgeApAAQAqAAAeAeQAdAdAAApQAAAqgdAdQgeAegqAAQgpAAgdgeQgegdAAgqg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.9,-12.9,25.9,25.9);


(lib.Tween2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EABE51").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA3AAAmAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.2,-13.2,26.5,26.5);


(lib.Tween1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EABE51").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA3AAAmAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.2,-13.2,26.5,26.5);


(lib.Tween7_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#04A7BF").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA3AAAmAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");
	this.shape_1.setTransform(0,0,1.189,1.189);

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.7,-15.7,31.4,31.4);


(lib.Tween4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#EABE51").ss(5.5,0,1).p("AhkAAQAAgpAegdQAdgeApAAQAqAAAeAeQAdAdAAApQAAAqgdAdQgeAegqAAQgpAAgdgeQgegdAAgqg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.9,-12.9,25.9,25.9);


(lib.Tween3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#04A7BF").ss(5.5,0,1).p("AhkAAQAAgpAegdQAdgeApAAQAqAAAeAeQAdAdAAApQAAAqgdAdQgeAegqAAQgpAAgdgeQgegdAAgqg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-12.9,-12.9,25.9,25.9);


(lib.Tween2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA3AAAmAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.2,-13.2,26.5,26.5);


(lib.Tween1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#04A7BF").s().p("AhcBdQgngnAAg2QAAg1AngnQAngnA1AAQA3AAAmAnQAnAnAAA1QAAA2gnAnQgnAng2AAQg1AAgngng");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-13.2,-13.2,26.5,26.5);


(lib.yellow = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// FlashAICB copy 4
	this.instance = new lib.Tween3("synched",0);
	this.instance.setTransform(998.4,653.4);
	this.instance._off = true;

	this.instance_1 = new lib.Tween4("synched",0);
	this.instance_1.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},167).to({state:[{t:this.instance_1}]},220).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(167).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(1));

	// FlashAICB copy 3
	this.instance_2 = new lib.Tween7("synched",0);
	this.instance_2.setTransform(998.4,653.4);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(143).to({_off:false},0).to({guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490,396.9,490.8,400.9], orient:'auto'}},220).to({_off:true},1).wait(24));

	// FlashAICB copy 2
	this.instance_3 = new lib.Tween3("synched",0);
	this.instance_3.setTransform(998.4,653.4);
	this.instance_3._off = true;

	this.instance_4 = new lib.Tween4("synched",0);
	this.instance_4.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_3}]},119).to({state:[{t:this.instance_4}]},220).to({state:[{t:this.instance_4}]},1).to({state:[]},1).wait(47));
	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(119).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(49));

	// yellow 1 copy
	this.instance_5 = new lib.Tween1("synched",0);
	this.instance_5.setTransform(998.4,653.4);
	this.instance_5._off = true;

	this.instance_6 = new lib.Tween2("synched",0);
	this.instance_6.setTransform(490.5,399.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_5}]},95).to({state:[{t:this.instance_6}]},220).to({state:[]},3).wait(70));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(95).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,489.3,396.4,490.4,399.4], orient:'auto'}},220).wait(73));

	// FlashAICB copy
	this.instance_7 = new lib.Tween3("synched",0);
	this.instance_7.setTransform(998.4,653.4);
	this.instance_7._off = true;

	this.instance_8 = new lib.Tween4("synched",0);
	this.instance_8.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_7}]},71).to({state:[{t:this.instance_8}]},220).to({state:[]},1).wait(96));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(71).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(97));

	// FlashAICB
	this.instance_9 = new lib.Tween7("synched",0);
	this.instance_9.setTransform(998.4,653.4);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(47).to({_off:false},0).to({guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490,396.9,490.8,400.9], orient:'auto'}},220).to({_off:true},1).wait(120));

	// FlashAICB
	this.instance_10 = new lib.Tween3("synched",0);
	this.instance_10.setTransform(998.4,653.4);
	this.instance_10._off = true;

	this.instance_11 = new lib.Tween4("synched",0);
	this.instance_11.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_10}]},23).to({state:[{t:this.instance_11}]},220).to({state:[]},1).wait(144));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(23).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(145));

	// yellow 1
	this.instance_12 = new lib.Tween1("synched",0);
	this.instance_12.setTransform(1002,651.4);

	this.instance_13 = new lib.Tween2("synched",0);
	this.instance_13.setTransform(490.5,399.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_12}]}).to({state:[{t:this.instance_13}]},220).to({state:[]},1).wait(167));
	this.timeline.addTween(cjs.Tween.get(this.instance_12).to({_off:true,guide:{path:[1002.4,651.7,935.5,732.8,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501.2,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,489.3,396.4,490.4,399.4], orient:'auto'}},220).wait(168));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(988.8,638.2,26.5,26.5);


(lib.yellow_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// FlashAICB copy 4
	this.instance_14 = new lib.Tween3_1("synched",0);
	this.instance_14.setTransform(998.4,653.4);
	this.instance_14._off = true;

	this.instance_15 = new lib.Tween4_1("synched",0);
	this.instance_15.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_14}]},167).to({state:[{t:this.instance_15}]},220).wait(1));
	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(167).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(1));

	// FlashAICB copy 3
	this.instance_16 = new lib.Tween7_1("synched",0);
	this.instance_16.setTransform(998.4,653.4);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(143).to({_off:false},0).to({guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490,396.9,490.8,400.9], orient:'auto'}},220).to({_off:true},1).wait(24));

	// FlashAICB copy 2
	this.instance_17 = new lib.Tween3_1("synched",0);
	this.instance_17.setTransform(998.4,653.4);
	this.instance_17._off = true;

	this.instance_18 = new lib.Tween4_1("synched",0);
	this.instance_18.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_17}]},119).to({state:[{t:this.instance_18}]},220).to({state:[{t:this.instance_18}]},1).to({state:[]},1).wait(47));
	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(119).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(49));

	// yellow 1 copy
	this.instance_19 = new lib.Tween1_1("synched",0);
	this.instance_19.setTransform(998.4,653.4);
	this.instance_19._off = true;

	this.instance_20 = new lib.Tween2_1("synched",0);
	this.instance_20.setTransform(490.5,399.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_19}]},95).to({state:[{t:this.instance_20}]},220).to({state:[]},3).wait(70));
	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(95).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,489.3,396.4,490.4,399.4], orient:'auto'}},220).wait(73));

	// FlashAICB copy
	this.instance_21 = new lib.Tween3_1("synched",0);
	this.instance_21.setTransform(998.4,653.4);
	this.instance_21._off = true;

	this.instance_22 = new lib.Tween4_1("synched",0);
	this.instance_22.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_21}]},71).to({state:[{t:this.instance_22}]},220).to({state:[]},1).wait(96));
	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(71).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(97));

	// FlashAICB
	this.instance_23 = new lib.Tween7_1("synched",0);
	this.instance_23.setTransform(998.4,653.4);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(47).to({_off:false},0).to({guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490,396.9,490.8,400.9], orient:'auto'}},220).to({_off:true},1).wait(120));

	// FlashAICB
	this.instance_24 = new lib.Tween3_1("synched",0);
	this.instance_24.setTransform(998.4,653.4);
	this.instance_24._off = true;

	this.instance_25 = new lib.Tween4_1("synched",0);
	this.instance_25.setTransform(491.6,401.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_24}]},23).to({state:[{t:this.instance_25}]},220).to({state:[]},1).wait(144));
	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(23).to({_off:false},0).to({_off:true,guide:{path:[1000,654.6,933.9,733.7,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,490.4,397.3,491,402], orient:'auto'}},220).wait(145));

	// yellow 1
	this.instance_26 = new lib.Tween1_1("synched",0);
	this.instance_26.setTransform(1002,651.4);

	this.instance_27 = new lib.Tween2_1("synched",0);
	this.instance_27.setTransform(490.5,399.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_26}]}).to({state:[{t:this.instance_27}]},220).to({state:[]},1).wait(167));
	this.timeline.addTween(cjs.Tween.get(this.instance_26).to({_off:true,guide:{path:[1002.4,651.7,935.5,732.8,844.5,785.7,741.9,845.3,623.1,860.3,540,870.9,460.3,848.4,383,826.7,319.3,777.3,255.6,727.9,215.3,658.6,173.6,586.7,163.1,503.6,155.7,445.5,171.4,389.7,186.6,335.6,221.2,291,255.8,246.4,304.3,218.2,354.6,189,412.8,181.7,453.5,176.5,492.7,187.5,530.3,198.1,561.5,222.3,592.7,246.5,612.5,280.5,632.9,315.7,638.1,356.4,641.7,384.9,634,412.4,626.5,438.7,609.6,460.5,592.7,482.3,568.9,496.2,544.2,510.5,515.7,514.1,496,516.6,476.8,511.2,458.2,506,443,494.2,427.7,482.3,418,465.7,408,448.4,405.4,428.7,401.8,400.2,419.4,377.6,436.9,354.9,465.4,351.3,485.3,348.8,501.2,361.1,516.8,373.4,519.3,393.3,521.1,407.2,512.5,418.3,503.9,429.4,490.2,431.2,480.4,432.2,472.6,426.4,464.9,420.4,463.6,410.6,462.8,403.8,467,398.3,471.2,392.9,478,392,482.8,391.4,486.6,394.4,489.3,396.4,490.4,399.4], orient:'auto'}},220).wait(168));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(988.8,638.2,26.5,26.5);


// stage content:



(lib.Spiral_Final_Reworked_BK = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// middle
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#DEDEDE").s().p("AgeB0QgwgMgagtQgWgoAMgvQANgzApgYQArgYAvAOQAyAOAWAoQAaAtgNAtQgNAvgsAZQgdARgdAAQgPAAgPgEg");
	this.shape.setTransform(485,400);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#00A8BC").s().p("AhJEMQhwgfg5hjQgagtgIg4QgHg2ANgzQANg2AigsQAiguAygdQBkg5BvAeQBwAeA5BiQA5BkgcBsQgOA3ghAsQgiAvgyAcQhAAlhGAAQglAAgpgLg");
	this.shape_1.setTransform(485,400);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#DEDEDE").s().p("AgeB0QgwgMgagtQgXgoAMgvQAOgzApgYQArgYAwAOQAwAOAXAoQAbAtgOAtQgNAvgrAZQgeARgdAAQgPAAgPgEg");
	this.shape_2.setTransform(485,400);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).to({state:[{t:this.shape_1},{t:this.shape_2}]},387).wait(1));

	// blue
	this.instance = new lib.yellow_1();
	this.instance.setTransform(332.5,305.5,0.803,0.803,180,0,0,674.9,525.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(388));

	// Yellow
	this.instance_1 = new lib.yellow();
	this.instance_1.setTransform(897.4,606.1,0.803,0.803,0,0,0,1002.3,651.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(388));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(544.1,593.7,848.7,422.9);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
