(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 970,
	height: 800,
	fps: 24,
	color: "#EAEAEB",
	opacity: 1.00,
	manifest: []
};



lib.ssMetadata = [];


// symbols:



(lib.Symbol8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#04A7BF").s().p("AhOCYIAAkvICdAAIAAEvg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-15.2,15.9,30.6);


(lib.Symbol7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#04A7BF").s().p("AhOFdIAAq5ICdAAIAAK5g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-35,15.9,70);


(lib.Symbol6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#04A7BF").s().p("AhONSIAA6jICdAAIAAajg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-85,15.9,170.1);


(lib.Symbol5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#04A7BF").s().p("AhOH4IAAvvICdAAIAAPvg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-50.4,15.9,100.9);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#04A7BF").s().p("AhONUIAA6nICdAAIAAang");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-85.2,15.9,170.5);


(lib.Symbol2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#04A7BF").s().p("AhOGSIAAsjICdAAIAAMjg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-40.2,15.9,80.5);


(lib.Symbol8_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("AhOCYIAAkvICdAAIAAEvg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-15.2,15.9,30.6);


(lib.Symbol7_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("AhOFdIAAq5ICdAAIAAK5g");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-35,15.9,70);


(lib.Symbol6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("AhONSIAA6jICdAAIAAajg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-85,15.9,170.1);


(lib.Symbol5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("AhOH4IAAvvICdAAIAAPvg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-50.4,15.9,100.9);


(lib.Symbol3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("AhONUIAA6nICdAAIAAang");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-85.2,15.9,170.5);


(lib.Symbol2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("AhOGSIAAsjICdAAIAAMjg");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-40.2,15.9,80.5);


(lib.Symbol4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Symbol3();

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-85.2,15.9,170.5);


(lib.Symbol4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance_1 = new lib.Symbol3_1();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-7.9,-85.2,15.9,170.5);


(lib.Symbol1copy = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.Symbol7_1();
	this.instance.setTransform(-220.3,-0.3,1,0.014);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(87).to({_off:false},0).to({regY:-0.1,scaleY:2.43,y:-85.2},69).to({regY:0,scaleY:0.01,y:-0.3},70).wait(1));

	// Layer 6
	this.instance_1 = new lib.Symbol6_1();
	this.instance_1.setTransform(-180.3,-0.2,1,0.006);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(72).to({_off:false},0).to({scaleY:1,y:-84.8},69).to({scaleY:0.01,y:-0.2},70).to({_off:true},1).wait(15));

	// Layer 4
	this.instance_2 = new lib.Symbol5_1();
	this.instance_2.setTransform(-140.2,-0.1,1,0.003);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(57).to({_off:false},0).to({scaleY:1.69,y:85},69).to({scaleY:0,y:-0.1},70).to({_off:true},1).wait(30));

	// Layer 3
	this.instance_3 = new lib.Symbol4_1();
	this.instance_3.setTransform(-100.1,0.1,1,0.001);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(42).to({_off:false},0).to({scaleY:1,y:85.3},69).to({scaleY:0,y:0.1},70).to({_off:true},1).wait(45));

	// Layer 2
	this.instance_4 = new lib.Symbol2_1();
	this.instance_4.setTransform(-60,0,1,0.001);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(28).to({_off:false},0).to({scaleY:2.12,y:85.1},69).to({scaleY:0,y:0},70).to({_off:true},1).wait(59));

	// Layer 1
	this.instance_5 = new lib.Symbol8_1();
	this.instance_5.setTransform(-20,0.1,1,0.015);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(13).to({_off:false},0).to({regY:0.1,scaleY:5.58,y:85.2},70).to({regY:0,scaleY:0.02,y:0.1},69).to({_off:true},1).wait(74));

	// Layer 16
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAEAEB").s().p("AijDfIAejKIEpjzIAAFIIgxB1g");
	this.shape.setTransform(-15.2,-17.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EAEAEB").s().p("AiZDfIAejKIEVjzIAAFIIgdB1g");
	this.shape_1.setTransform(-16.2,-17.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EAEAEB").s().p("AiZDfIAejKID4jzIAdFIIgdB1g");
	this.shape_2.setTransform(-16.2,-17.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EAEAEB").s().p("AiZDfIAejKIDFjzIBQFIIgdB1g");
	this.shape_3.setTransform(-16.2,-17.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EAEAEB").s().p("AiZDfICem9ICVFIIgdB1g");
	this.shape_4.setTransform(-16.2,-17.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EAEAEB").s().p("AiZDfIBkm9IDPFIIgdB1g");
	this.shape_5.setTransform(-16.2,-17.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EAEAEB").s().p("AiKDfIBGm9IDQFIIAAB1g");
	this.shape_6.setTransform(-17.7,-17.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAEAEB").s().p("AiKDBIAAmBIEWEMIAAB1g");
	this.shape_7.setTransform(-17.7,-14.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EAEAEB").s().p("AiKCjIAAlFIEWDQIAAB1g");
	this.shape_8.setTransform(-17.7,-11.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EAEAEB").s().p("AiKCFIAAkJIEWCUIAAB1g");
	this.shape_9.setTransform(-17.7,-8.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EAEAEB").s().p("AiKBnIAAjNIEWBaIAABzg");
	this.shape_10.setTransform(-17.7,-5.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#EAEAEB").s().p("AiKBOIAAibIEWAoIAABzg");
	this.shape_11.setTransform(-17.7,-3.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EAEAEB").s().p("AiKAmIAAhLIEWAAIAABLg");
	this.shape_12.setTransform(-17.7,0.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#EAEAEB").s().p("AiKAmIAAhLIEWAAIAABLg");
	this.shape_13.setTransform(-17.7,0.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[]},1).to({state:[{t:this.shape_13}]},139).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},62).wait(1));

	// Layer 12
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#EABE51").s().p("AiOCLQABh2BShPQBPhRB1AAIAGAAIAACeIgGAAQgyAAgjAjQgkAiAAA0g");
	this.shape_14.setTransform(-13.6,-13.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(213).to({_off:true},1).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.7,-40.3,33,44.7);


(lib.blue2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 7
	this.instance = new lib.Symbol7();
	this.instance.setTransform(-220.3,-0.3,1,0.014);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(87).to({_off:false},0).to({regY:-0.1,scaleY:2.43,y:-85.2},69).to({regY:0,scaleY:0.01,y:-0.3},70).wait(1));

	// Layer 6
	this.instance_1 = new lib.Symbol6();
	this.instance_1.setTransform(-180.3,-0.2,1,0.006);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(72).to({_off:false},0).to({scaleY:1,y:-84.8},69).to({scaleY:0.01,y:-0.2},70).to({_off:true},1).wait(15));

	// Layer 4
	this.instance_2 = new lib.Symbol5();
	this.instance_2.setTransform(-140.2,-0.1,1,0.003);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(57).to({_off:false},0).to({scaleY:0.34,y:17.2},14).to({scaleY:1.69,y:85},55).to({scaleY:0,y:-0.1},70).to({_off:true},1).wait(30));

	// Layer 3
	this.instance_3 = new lib.Symbol4();
	this.instance_3.setTransform(-100.1,0.1,1,0.001);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(42).to({_off:false},0).to({regY:0.5,scaleY:0.1,y:8.8},7).to({regY:0,scaleY:1,y:85.3},62).to({scaleY:0,y:0.1},70).to({_off:true},1).wait(45));

	// Layer 2
	this.instance_4 = new lib.Symbol2();
	this.instance_4.setTransform(-60,0,1,0.001);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(28).to({_off:false},0).to({scaleY:0.34,y:13.6},11).to({scaleY:2.12,y:85.1},58).to({scaleY:0,y:0},70).to({_off:true},1).wait(59));

	// Layer 1
	this.instance_5 = new lib.Symbol8();
	this.instance_5.setTransform(-20,0.1,1,0.015);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(13).to({_off:false},0).to({regY:0.1,scaleY:0.41,y:6.2},5).to({scaleY:5.58,y:85.2},65).to({regY:0,scaleY:0.02,y:0.1},69).to({_off:true},1).wait(74));

	// Layer 16
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EAEAEB").s().p("AijDfIAejKIEpjzIAAFIIgxB1g");
	this.shape.setTransform(-15.2,-17.9);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EAEAEB").s().p("AiZDfIAejKIEVjzIAAFIIgdB1g");
	this.shape_1.setTransform(-16.2,-17.9);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EAEAEB").s().p("AiZDfIAejKID4jzIAdFIIgdB1g");
	this.shape_2.setTransform(-16.2,-17.9);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EAEAEB").s().p("AiZDfIAejKIDFjzIBQFIIgdB1g");
	this.shape_3.setTransform(-16.2,-17.9);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EAEAEB").s().p("AiZDfICem9ICVFIIgdB1g");
	this.shape_4.setTransform(-16.2,-17.9);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#EAEAEB").s().p("AiZDfIBkm9IDPFIIgdB1g");
	this.shape_5.setTransform(-16.2,-17.9);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#EAEAEB").s().p("AiKDfIBGm9IDQFIIAAB1g");
	this.shape_6.setTransform(-17.7,-17.9);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#EAEAEB").s().p("AiKDBIAAmBIEWEMIAAB1g");
	this.shape_7.setTransform(-17.7,-14.9);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#EAEAEB").s().p("AiKCjIAAlFIEWDQIAAB1g");
	this.shape_8.setTransform(-17.7,-11.9);

	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#EAEAEB").s().p("AiKCFIAAkJIEWCUIAAB1g");
	this.shape_9.setTransform(-17.7,-8.9);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#EAEAEB").s().p("AiKBnIAAjNIEWBaIAABzg");
	this.shape_10.setTransform(-17.7,-5.9);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f("#EAEAEB").s().p("AiKBOIAAibIEWAoIAABzg");
	this.shape_11.setTransform(-17.7,-3.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f("#EAEAEB").s().p("AiKAmIAAhLIEWAAIAABLg");
	this.shape_12.setTransform(-17.7,0.6);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#EAEAEB").s().p("AiKBnIAAjNIEWBaIAABzg");
	this.shape_13.setTransform(-17.7,-5.9);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape}]}).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_10}]},1).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_12}]},1).to({state:[]},1).to({state:[{t:this.shape_12}]},139).to({state:[{t:this.shape_11}]},1).to({state:[{t:this.shape_13}]},1).to({state:[{t:this.shape_9}]},1).to({state:[{t:this.shape_8}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape}]},1).to({state:[]},62).wait(1));

	// Layer 12
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#04A7BF").s().p("AiOCLQAAh2BThPQBQhRB0AAIAGAAIAACeIgGAAQgxAAgkAjQgkAiAAA0g");
	this.shape_14.setTransform(-13.6,-12.4);

	this.timeline.addTween(cjs.Tween.get(this.shape_14).wait(213).to({_off:true},1).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-31.7,-40.3,33,44.7);


// stage content:



(lib.Equalizer_Final_Reworked_V05_BK = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.blue2();
	this.instance.setTransform(499,391.5,1,1,180);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16));

	// Layer 2
	this.instance_1 = new lib.Symbol1copy();
	this.instance_1.setTransform(371.8,450.7,1,1,0,0,0,-113.8,51.3);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(16));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(938.9,759.1,76.8,72.6);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
