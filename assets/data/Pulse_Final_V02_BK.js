(function (lib, img, cjs, ss) {

var p; // shortcut to reference prototypes
lib.webFontTxtFilters = {};

// library properties:
lib.properties = {
	width: 970,
	height: 800,
	fps: 24,
	color: "#EAEAEB",
	opacity: 1.00,
	webfonts: {},
	manifest: []
};



lib.ssMetadata = [];


lib.webfontAvailable = function(family) {
	lib.properties.webfonts[family] = true;
	var txtFilters = lib.webFontTxtFilters && lib.webFontTxtFilters[family] || [];
	for(var f = 0; f < txtFilters.length; ++f) {
		txtFilters[f].updateCache();
	}
};
// symbols:



(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#EABE51").s().p("AEGCSQAAhthNhLQhNhNhsAAQhrAAhNBNQhNBLAABtIgeAAQAAh5BWhUQBVhWB4AAQB5AABWBWQBVBUAAB5g");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-29.2,-14.6,58.5,29.3);


(lib.Tween5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#2BBBC7").s().p("AEGCSQAAhthNhLQhNhNhsAAQhrAAhNBNQhNBLAABtIgeAAQAAh5BWhUQBVhWB4AAQB5AABWBWQBVBUAAB5g");

	this.timeline.addTween(cjs.Tween.get(this.shape_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-29.2,-14.6,58.5,29.3);


(lib.Symbol1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Tween5("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-29.2,-14.6,58.5,29.3);


(lib.Symbol1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance_1 = new lib.Tween5_1("synched",0);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-29.2,-14.6,58.5,29.3);


(lib.blue = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy 4
	this.instance = new lib.Symbol1_1();
	this.instance.setTransform(0.2,-14.9);
	this.instance.alpha = 0.801;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(96).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(2));

	// Layer 1 copy 3
	this.instance_1 = new lib.Symbol1_1();
	this.instance_1.setTransform(0.2,-14.9);
	this.instance_1.alpha = 0.801;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(72).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},5).wait(20));

	// Layer 1 copy 2
	this.instance_2 = new lib.Symbol1_1();
	this.instance_2.setTransform(0.2,-14.9);
	this.instance_2.alpha = 0.801;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(48).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},5).wait(44));

	// Layer 1 copy
	this.instance_3 = new lib.Symbol1_1();
	this.instance_3.setTransform(0.2,-14.9);
	this.instance_3.alpha = 0.801;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(24).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},1).wait(72));

	// Layer 1
	this.instance_4 = new lib.Symbol1_1();
	this.instance_4.setTransform(0.2,-14.9);
	this.instance_4.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},1).wait(96));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-29.1,-29.5,58.5,29.3);


(lib.Symbol3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1 copy 4
	this.instance = new lib.Symbol1();
	this.instance.setTransform(0.2,-14.9);
	this.instance.alpha = 0.801;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(96).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(2));

	// Layer 1 copy 3
	this.instance_1 = new lib.Symbol1();
	this.instance_1.setTransform(0.2,-14.9);
	this.instance_1.alpha = 0.801;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(72).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},22).wait(3));

	// Layer 1 copy 2
	this.instance_2 = new lib.Symbol1();
	this.instance_2.setTransform(0.2,-14.9);
	this.instance_2.alpha = 0.801;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(48).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},25).wait(24));

	// Layer 1 copy
	this.instance_3 = new lib.Symbol1();
	this.instance_3.setTransform(0.2,-14.9);
	this.instance_3.alpha = 0.801;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(24).to({_off:false},0).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},1).wait(72));

	// Layer 1
	this.instance_4 = new lib.Symbol1();
	this.instance_4.setTransform(0.2,-14.9);
	this.instance_4.alpha = 0.801;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({regY:0.1,scaleX:2.23,scaleY:2.23,y:-32.7,alpha:0.602},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:4.26,scaleY:4.26,y:-63.8,alpha:0.398},23,cjs.Ease.get(0.4)).wait(1).to({scaleX:7.79,scaleY:7.79,y:-113.6,alpha:0.199},23,cjs.Ease.get(0.4)).wait(1).to({regX:0.1,scaleX:11.18,scaleY:11.18,x:1.3,y:-161.9,alpha:0},23,cjs.Ease.get(0.4)).wait(1).to({_off:true},1).wait(96));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-29.1,-29.5,58.5,29.3);


// stage content:



(lib.Pulse_Final_V02_BK = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// middle
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#33BAC9").s().p("AjMA5QhXhMgLh1IChAAQALAyAnAgQApAhAyAAQAzAAAoghQAoggAKgyICiAAQgLB1hXBMQhYBQh1AAQh1AAhXhQg");
	this.shape.setTransform(485,416.8);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#EABE51").s().p("ACNCJQgKgxgoghQgoghgzAAQgyAAgpAhQgnAhgLAxIihAAQALh0BXhNQBXhQB1AAQB1AABYBQQBXBNALB0g");
	this.shape_1.setTransform(485,383.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1,p:{x:485}},{t:this.shape,p:{x:485}}]}).to({state:[{t:this.shape_1,p:{x:485.3}},{t:this.shape,p:{x:485.3}}]},97).wait(1));

	// white middle
	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#EAEAEB").s().p("AjVDWQhZhZAAh9QAAh8BZhZQBZhZB8AAQB9AABZBZQBZBZAAB8QAAB9hZBZQhZBZh9AAQh8AAhZhZg");
	this.shape_2.setTransform(485.3,399.8);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#EABE51").s().p("AEGCSQAAhthNhLQhNhNhsAAQhrAAhNBNQhNBLAABtIgeAAQAAh5BWhUQBVhWB4AAQB5AABWBWQBVBUAAB5g");
	this.shape_3.setTransform(485.2,385.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_3},{t:this.shape_2}]}).to({state:[{t:this.shape_3},{t:this.shape_2}]},97).wait(1));

	// white bar
	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#EAEAEB").s().p("EgiSADhIAAnAMBElAAAIAAHAg");
	this.shape_4.setTransform(484.6,399.6,1.82,0.378);

	this.timeline.addTween(cjs.Tween.get(this.shape_4).wait(98));

	// blue
	this.instance = new lib.blue();
	this.instance.setTransform(485.2,398.7,1,1,180);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(98));

	// yellow
	this.instance_1 = new lib.Symbol3();
	this.instance_1.setTransform(484.9,399.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(98));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(570.1,769.5,799.2,61);

})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{});
var lib, images, createjs, ss;
