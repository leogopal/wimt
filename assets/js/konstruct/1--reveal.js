// KONSTRUCT: REVEAL ###########################################################

'use strict';

module.exports = function() {

  jQuery(document).ready(function($) {

    $(".reveal").click(function(e) {

      var target = $(this).attr('href');
      var video = $("#modal-media > .modal-body > iframe").attr("src");

      if (!$(target).hasClass('is-open')) {

        $(target).addClass('is-open');
        $(".dissapear").addClass('is-open');

      } else {

        $(target).removeClass('is-open');
        $(".dissapear").removeClass('is-open');


        $("#modal-media > .modal-body > iframe").attr("src","");
        $("#modal-media > .modal-body > iframe").attr("src",video);

        console.log('changing video thing.')

      }

      if (!$(target).hasClass('-open')) {

        $(target).addClass('-open');
        $(".dissapear").hide();

      } else {

        $(target).removeClass('-open');
        $(".dissapear").show();

      }

      e.preventDefault();

    });

  });

}

// FOUND A BUG?:
//
//   If you found a bug or something is broken, please do me a favour and log it
//   over at: https://github.com/konstruct/konstruct.js/issues

// END OF FILE #################################################################
