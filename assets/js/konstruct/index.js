// KONSTRUCT ###################################################################

// == INDEX  ===================================================================
// ==
// == 1. DEPENDENCIES
// == 2. KONSTRUCT MODULE
// ==
// =============================================================================

'use strict';

// 1. DEPENDENCIES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

var WebFont = require('webfontloader');

// 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 2. KONSTRUCT MODULE +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module.exports = function(config) {

  // 2.1. LOAD FONTS -----------------------------------------------------------

  WebFont.load(config.font);

  // 2.1. END ------------------------------------------------------------------

  // 2.2. SCRIPTS --------------------------------------------------------------

  // 2.2.1. REVEAL

  if(config.scripts.reveal){
    var reveal = require('./1--reveal.js');
    reveal();
  }

  // 2.2.1. END

  // 2.2.2. masthead

  if(config.scripts.scroller){
    var scroller = require('./3--scroller.js');
    scroller(config.scripts.scroller);
  }

  // 2.2.2. END

  // 2.2. END ------------------------------------------------------------------

}

// 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// FOUND A BUG?:
//
//   If you found a bug or something is broken, please do me a favour and log it
//   over at: https://github.com/konstruct/konstruct.js/issues

// END OF FILE #################################################################
