// KONSTRUCT: MASTHEAD #########################################################

'use strict';

module.exports = function(config) {

  jQuery(document).ready(function($) {

    $(window).scroll(function(e) {

      if (window.pageYOffset > config.offset) {

        $(config.element).addClass(config.class);

      } else {

        $(config.element).removeClass(config.class);

      }

    });

  });

}

// FOUND A BUG?:
//
//   If you found a bug or something is broken, please do me a favour and log it
//   over at: https://github.com/konstruct/konstruct.js/issues

// END OF FILE #################################################################
