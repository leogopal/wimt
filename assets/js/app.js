// SCRIPTS #####################################################################

// == INDEX  ===================================================================
// ==
// == 1. LOAD DEPENDENCIES
// == 2. GLOBAL VARIABLES
// == 3. LOAD KONSTRUCT
// == 4. LOAD SCRIPTS
// ==
// =============================================================================

'use strict';

console.log('')

console.log('ooooooooooooooooo+`                           +ooooooooooooooooo.                 ``````````````````');
console.log('+ooooooooooooooooo-                          `oooooooooooooooooo`                 ------------------');
console.log(':ooooooooooooooooo+`                         /ooooooooooooooooo+                 `------------------');
console.log('.oooooooooooooooooo+`                      `/oooooooooooooooooo-                `------------------.');
console.log(':oooooooooooooooooo+-                    .+oooooooooooooooooo+`               `-------------------`');
console.log('`+ooooooooooooooooooo+-`              `./oooooooooooooooooooo.              `.-------------------. ');
console.log('  `+ooooooooooooooooooooo/:.`      `.-/oooooooooooooooooooooo-            ``---------------------.  ');
console.log('   `+ooooooooooooooooooooooooooooooooooooooooooooooooooooooo/---..````..-------------------------   ');
console.log('    `/ooooooooooooooooooooooooooooooooooooooooooooooooooooo:-----------------------------------.    ');
console.log('      -+ooooooooooooooooooooooooooooooooooooooooooooooooo/:-----------------------------------`     ');
console.log('       `:oooooooooooooooooooooooooooooooooooooooooooooo+:-----------------------------------.       ');
console.log('         `-+ooooooooooooooooooooooooooooooooooooooooo+:-----------------------------------.`        ');
console.log('            ./oooooooooooooooooooooooooooooooooooo+/:-----------------------------------.           ');
console.log('               .:+oooooooooooooooooooooooooooo+/-`.----------------------------------.`             ');
console.log('                  `.-/+oooooooooooooooooo+/:.`       `.--------------------------.``                ');
console.log('                        ``.----------..`                 ``...-------------..``              ');

// 1. LOAD DEPENDENCIES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

window.jQuery = window.$ = require('jquery');
var konstruct = require('./konstruct');
var scripts = require('./scripts');

// 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 2. GLOBAL VARIABLES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

var env = 'DEV';
var browser = {
  width: $(window).width(),
  height: $(window).height()
}

// 2. GLOBAL VARIABLES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 3. KONSTRUCT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

konstruct({
  font: {
    google: {
      families: [
        'Raleway:300,400, 400i,500,700',
        'Open+Sans:300,400',
        'Bitter:400i',
      ]
    }
  },
  scripts: {
    reveal: true,
    scroller: {
      offset: 80,
      element: '.masthead',
      class: '-scrolled'
    }
  }

});

// 3. GLOBAL VARIABLES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 4. APP ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

scripts();

// 4. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// FOUND A BUG?:
//
//   If you found a bug or something is broken, please do me a favour and log it
//   over at: https://github.com/konstruct/konstruct.js/issues

// END OF FILE #################################################################
