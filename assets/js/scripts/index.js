// SCRIPTS #####################################################################

// == INDEX  ===================================================================
// ==
// == 1. DEPENDENCIES
// == 2. APP MODULE
// ==
// =============================================================================

'use strict';

// 1. DEPENDENCIES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

require('fullpage.js');

var carousel_awards = require('./1--awards.js');
var carousel_case_studies = require('./2--case-studies.js');
var animations = require('./3--animations.js');
var carousel = require('./5--carousel.js');

// 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 2. APP MODULE +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module.exports = function(config) {

  // 2.1. FULLPAGE.JS ----------------------------------------------------------

  $(document)
    .ready(function(config) {

      if ($(window)
        .width() > 1024) {

        $('#fullpage')
          .fullpage({
            anchors: [
              'hello',
              'whats-wrong-with-transport',
              'one-movement',
              'thats-what-we-do',
              'connect',
              'real-time',
              'learn',
              'integrated',
              'solutions',
              'case-studies',
              'partners',
              'contact-us',
              'end'
            ],

            scrollBar: true,
            controlArrows: false,
            sectionSelector: '.c-panel',
            slideSelector: '.c-slide',
            slidesNavigation: false,
            afterRender: function() {
              setInterval(function() {
                $.fn.fullpage.moveSlideRight();
              }, 6000);
            }
          });

      }

    });

  // 2.1. END ------------------------------------------------------------------

  // 2.2. AWARDS ---------------------------------------------------------------

  $(document)
    .ready(function() {

      $('#quote-1')
        .addClass('-open');

      $(".reveal-quotes")
        .hover(function(e) {

          var target = $(this)
            .attr('href');

          $('p')
            .removeClass('-open');

          if (!$(target)
            .hasClass('-open')) {

            $(target)
              .addClass('-open');

          } else {

            $(target)
              .removeClass('-open');

          }

          e.preventDefault();

        });

    });

  // 2.2. END ------------------------------------------------------------------

  // 2.3. ANIMATIONS -----------------------------------------------------------

  carousel_case_studies();
  animations();
  carousel();

  // 2.3. END ------------------------------------------------------------------

}

// 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// FOUND A BUG?:
//
//   If you found a bug or something is broken, please do me a favour and log it
//   over at: https://github.com/konstruct/konstruct.js/issues

// END OF FILE #################################################################
