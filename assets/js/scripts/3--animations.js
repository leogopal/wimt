// KONSTRUCT ###################################################################

'use strict';

module.exports = function() {

  jQuery(document)
    .ready(function($) {

      // 1. FUNCTION +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      function isIE() {
        var rv = false; // Return value assumes failure.

        if (/MSIE 10/i.test(navigator.userAgent)) {
          // This is internet explorer 10
          rv = true;
        }

        if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
          // This is internet explorer 9 or 11
          rv = true;
        }

        if (/Edge\/\d./i.test(navigator.userAgent)) {
          // This is Microsoft Edge
          rv = true;
        }

        return rv;
      }



      if (isIE()) {

        var path = 'https://www.whereismytransport.com/wp-content/themes/wimt/assets/video';

        $("img[src$='icon-spiral.svg']")
          .after('<img src="' + path + '/spiral.gif" />');
        $("img[src$='icon-spiral.svg']")
          .hide();

        $("img[src$='icon-pulse.svg']")
          .after('<img src="' + path + '/pulse.gif" />');
        $("img[src$='icon-pulse.svg']")
          .hide();

        $("img[src$='icon-equaliser.svg']")
          .after('<img src="' + path + '/equaliser.gif" />');
        $("img[src$='icon-equaliser.svg']")
          .hide();

      } else {
        var path = 'https://www.whereismytransport.com/wp-content/themes/wimt/assets/data';
        $("img[src$='icon-spiral.svg']")
          .after('<iframe src="' + path + '/animate-spiral.html"></iframe>');
        $("img[src$='icon-spiral.svg']")
          .hide();

        $("img[src$='icon-pulse.svg']")
          .after('<iframe src="' + path + '/animate-pulse.html"></iframe>');
        $("img[src$='icon-pulse.svg']")
          .hide();

        $("img[src$='icon-equaliser.svg']")
          .after('<iframe src="' + path + '/animate-equaliser.html"></iframe>');
        $("img[src$='icon-equaliser.svg']")
          .hide();
      }

      console.log(isIE());

      // 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      // 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

      //$('.m-layout--animated > ul > li').index().addClass('active');
      //console.log( );

      var counter = 0;
      $('.m-layout--animated > .container > ul > li:eq(0)')
        .addClass('active');

      var blokkie = function() {

        setTimeout(function() {

          $('.m-layout--animated > .container > ul > li')
            .removeClass('active')

          $('.m-layout--animated > .container > ul > li:eq(' + counter + ')')
            .addClass('active')

          counter++;

          if (counter > 2) {
            counter = 0;
          }

          blokkie();

        }, 2000);

      }

      if ($('.m-layout--animated > .container')
        .length) {
        blokkie();
      }


      // 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    });

}

// END OF FILE #################################################################
