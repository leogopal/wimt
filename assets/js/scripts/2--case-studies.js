// KONSTRUCT ###################################################################

'use strict';

// 1. REQUIRE LIBRARIES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

require('slick-carousel');

// 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 2. AWARD SCROLLER +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module.exports = function( ) {

  jQuery(document).ready(function($) {

    var container = $('.c-carousel');

    // 3. CONTROLS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    container.slick({
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      centerMode: true,
      variableWidth: true,
      prevArrow: '<button type="button" class="slick-prev"></button>',
      nextArrow: '<button type="button" class="slick-next"></button>'
    });

    // 3. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  });

}

// 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// FOUND A BUG?:
//
//   If you found a bug or something is broken, please do me a favour and log it
//   over at: https://github.com/konstruct/konstruct.js/issues

// END OF FILE #################################################################
