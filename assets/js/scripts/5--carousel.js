// KONSTRUCT ###################################################################

'use strict';

// 1. REQUIRE LIBRARIES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// 2. AWARD SCROLLER +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

module.exports = function( ) {

  jQuery(document).ready(function($) {

    var container = $('.c-carousely');
    var slide = $('.c-carousely li');
    var thumb = $('.c-carousely-thumbs li');
    var count = slide.length;
    var counter = 0;

    slide.eq(0).addClass('active');
    thumb.eq(0).addClass('active');

    $('.next').click(function(e) {

      slide.removeClass('active');
      thumb.removeClass('active');

      if ( counter < (count - 1) ) {

        counter ++;

        slide.eq(counter).addClass('active');
        thumb.eq(counter).addClass('active');

      } else {
        counter = 0;
        slide.eq(counter).addClass('active');
        thumb.eq(counter).addClass('active');
      }

      e.preventDefault();

    });

    $('.previous').click(function(e) {

      slide.removeClass('active');
      thumb.removeClass('active');

      if ( counter > 0 ) {

        counter --;

        slide.eq(counter).addClass('active');
        thumb.eq(counter).addClass('active');

      } else {
        counter = 0;
        slide.eq(counter).addClass('active');
        thumb.eq(counter).addClass('active');
      }

      e.preventDefault();

    });
  });

}

// 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// FOUND A BUG?:
//
//   If you found a bug or something is broken, please do me a favour and log it
//   over at: https://github.com/konstruct/konstruct.js/issues

// END OF FILE #################################################################
