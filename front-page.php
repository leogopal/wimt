<?php
/*
 * Template Name: Blog
 * Description: Blog for WIMT with flexible content fields.
 */

?>
<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <!-- 2.1. SLIDES - - - - - - - - - - - - - - - - - - - - - - - - - - - - -->

    <section class="mast" id="fullpage">

      <?php
      if( have_rows('wimt_content') ):
        while ( have_rows('wimt_content') ) : the_row();
      ?>

        <!-- 2.1.1. TEXT SLIDES -->

        <?php if ( get_row_layout() == 'block_text' ) { ?>

          <div class="c-panel <?php the_sub_field('layout') ?>" >

          <?php if ( get_sub_field('layout') == 'slide-partners' ) { ?>
              <?php get_template_part('partials/component', 'text'); ?>
              <?php get_template_part('partials/section', 'flash'); ?>
          <?php } else { ?>

              <?php get_template_part('partials/component', 'text'); ?>

          <?php } ?>

          </div>

        <?php }; ?>

        <!-- 2.1.1. END -->

        <!-- 2.1.2. LIST SLIDES -->

        <?php if ( get_row_layout() == 'block_content_list' ) { ?>

          <?php get_template_part('partials/component', 'hp-list'); ?>

        <?php }; ?>

        <!-- 2.1.2. END -->

        <!-- 2.1.1. LINKED CONTENT SLIDES -->

        <?php if ( get_row_layout() == 'block_linked' && get_sub_field('content_type') == 'case_study' ) { ?>

          <div class="c-panel slide-case_study" >
            <?php get_template_part('partials/component', 'linked'); ?>
          </div>
        <?php }; ?>

        <!-- 2.1.1. END -->

        <?php if ( get_row_layout() == 'block_content_grid' ) { ?>

          <?php get_template_part('partials/component', 'hp-blocks') ?>

        <?php }; ?>

        <!-- 2.1.1. END -->

      <?php
        endwhile; // close the loop of flexible content
      endif; // close flexible content conditional
      ?>

      <div class="c-panel -split slide-cta" >

        <?php
        if( have_rows('wimt_content') ):
          while ( have_rows('wimt_content') ) : the_row();
        ?>

          <!-- 2.1.1. LINKED CONTENT SLIDES -->

          <?php if ( get_row_layout() == 'block_cta' ) { ?>
              <?php get_template_part('partials/component', 'cta'); ?>
          <?php }; ?>

          <!-- 2.1.1. END -->

        <?php
          endwhile; // close the loop of flexible content
        endif; // close flexible content conditional
        ?>

      </div>

      <div class="c-panel  slide-footer">

        <?php
        if( have_rows('wimt_content') ):
          while ( have_rows('wimt_content') ) : the_row();
        ?>

        <?php if ( get_row_layout() == 'block_linked' && get_sub_field('content_type') == 'award' ) { ?>
            <?php get_template_part('partials/component', 'linked'); ?>
        <?php }; ?>

          <!-- 2.1.1. LINKED CONTENT SLIDES -->

        <?php if ( get_row_layout() == 'block_mailing_list' ) { ?>
            <?php get_template_part('partials/component', 'mailing-list'); ?>
        <?php }; ?>

          <!-- 2.1.1. END -->

        <?php
          endwhile; // close the loop of flexible content
        endif; // close flexible content conditional
        ?>

        <?php get_template_part('partials/section', 'footer'); ?>

      </div>

    </section>

    <!-- 2.1. END - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

    <!-- 2.2. VIDEO - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

    <div class="background">

      <video src="<?php bloginfo('template_directory'); ?>/assets/video/video.mp4" poster="<?php bloginfo('template_directory'); ?>/assets/images/site/FullMap.jpg" preload autoplay loop ></video>

    </div>

    <!-- 2.2. END - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
