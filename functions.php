<?php

# 1. WIDGET REGIONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if ( function_exists('register_sidebar') ) {

}

# 1. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 2. MENUS +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

add_action( 'init', 'wimt_navigation' );

function wimt_navigation() {
  register_nav_menus(
    array(
      'primary-nav' => __( 'Main Nav' ),
      'blog-nav' => __( 'Blog Nav' ),
      'footer-nav-1' => __( 'Footer Nav: 1' ),
      'footer-nav-2' => __( 'Footer Nav: 2' ),
      'footer-nav-3' => __( 'Footer Nav: 3' ),
      'footer-nav-4' => __( 'Footer Nav: 4' )
    )
  );
}

# 2. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 3. CONTENT TYPES +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

require get_template_directory() . '/inc/content-types.php';

# 3. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 4. ADVANCED CUSTOM FIELDS ++++++++++++++++++++++++++++++++++++++++++++++++++++

# 4.1. THEME OPTIONS PAGE ------------------------------------------------------

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(

    'page_title' 	=> 'WIMT General Settings',
		'menu_title'	=> 'WIMT Settings',
		'menu_slug' 	=> 'wimt-general-settings',
		'redirect'		=> false

  ));

  acf_add_options_page(array(

    'page_title' 	=> 'WIMT Flash Message',
		'menu_title'	=> 'WIMT Flash Message',
		'menu_slug' 	=> 'wimt-flash-message',
		'redirect'		=> false

  ));

}

# 4.1. END ---------------------------------------------------------------------

# 4.2. ADD GOOGLE API KEYS -----------------------------------------------------

function my_acf_init() {

	acf_update_setting('google_api_key', 'AIzaSyDS_mhNxJbknGljbdScvtwtY2HAli0XC7o');
}

add_action('acf/init', 'my_acf_init');

# 4.2. END ---------------------------------------------------------------------

# 4.2. END ---------------------------------------------------------------------

add_action( 'acf/register_fields', 'my_register_fields' );

function my_register_fields() {

     include_once( 'acf-post-type-selector}/acf-post-type-selector.php' );

}

# 4.2. END ---------------------------------------------------------------------

# 4. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 5. HELPER FUNCTIONS ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 5.1. UPLOAD SVG FORMATS ------------------------------------------------------

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

# 5.1. END ---------------------------------------------------------------------

# 5.2. ADD TITLE TAG TO WORDPRESS ----------------------------------------------

add_theme_support( 'title-tag' );
add_theme_support( 'description-tag' );
add_theme_support( 'keywords-tag' );

# 5.2. END ---------------------------------------------------------------------

# 5.3. TEMPLATE TAGS -----------------------------------------------------------

function wimt_pagination(){
    global $query;
    echo paginate_links();
    echo "HELLO!";
}

# 5.3. END ---------------------------------------------------------------------

# 5.4. SEARCH ------------------------------------------------------------------

/* Plugin Name: Parameter
 Plugin URI: http://webopius.com/
 Description: A plugin to allow parameters to be passed in the URL and recognized by WordPress
 Author: Adam Boyse
 Version: 1.0
 Author URI: http://www.webopius.com/
 */
add_filter('query_vars', 'parameter_queryvars' );
function parameter_queryvars( $qvars )
{
	$qvars[] = st;
	return $qvars;
}

# 5.4. END ---------------------------------------------------------------------

# 5. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 6. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   *
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /**
   * We construct the pagination arguments to enter into our paginate_links
   * function.
   */
  $pagination_args = array(
    'base'            => preg_replace('/\?.*/', '/', get_pagenum_link(1)) . '%_%',
    'format'          => '/page/%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('<'),
    'next_text'       => __('>'),
    'type'            => 'plain',
    'add_args' => array(
            's' => get_query_var('s')
    ),
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='m-listings-nav'>";
      echo $paginate_links;
    echo "</nav>";
  }

}

# 6. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 7. SHORTCODES ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

# 7.1. QUOTE ...................................................................

function sc_pullquote( $atts , $content = null ) {
  extract(shortcode_atts(array(
    'large' => false,
  ), $atts));

  if ($large) {
    $style = 'large';
  }

  return '<blockquote class="c-pullquote c-pullquote--' . $style .'"><p>"' . $content . '"</p><div class="c-pullquote-tooltip"><a href="https://twitter.com/home?status=' . $content .' - via @mytransport" target="_blank"><img src="' . site_url() .'/wp-content/themes/wimt/assets/images/icons/icon--social-twitter-white.svg" class="c-icon -small"></a></div></blockquote>';
}
add_shortcode( 'quote', 'sc_pullquote' );

# 7.1. END .....................................................................

# 7.2. QUOTE ...................................................................

function sc_read( $atts ) {
  extract(shortcode_atts(array(
    'type' => 'post',
    'slug' => '',
  ), $atts));

  $el_post = get_page_by_path( $slug, OBJECT, $type );
  //var_dump($el_post);

  return '<aside class="c-read"><strong>Read: <a href="' . site_url() .'?p='. $el_post->ID .'">' . $el_post->post_title . '</a></strong></aside>';

}
add_shortcode( 'read', 'sc_read' );

# 7.2. END .....................................................................

# 7.3. MAILING LIST ............................................................

function sc_mailing_list( $atts , $content = null ) {
  extract(shortcode_atts(array(
    'large' => false,
  ), $atts));

  return '<div class="section form m-signup m-signup--content">

  <div class="container">

    <header class="form-header">
      <p>' .  get_field('weekly_list', 'option')  . '</p>
    </header>

    <div class="m-signup-form">

      <script type="text/javascript">var loc = "https://analytics-eu.clickdimensions.com/whereismytransportcom-afl02/pages/";</script>
      <script type="text/javascript" src="https://az551914.vo.msecnd.net/web/v10/CDWidget.js"></script>
      <div pageID="ufwbqviueeaa3cq0a6woaa"></div>

    </div>

  </div>

</div>';
}
add_shortcode( 'mailing-list', 'sc_mailing_list' );

# 7.3. END .....................................................................

# 7.4. EBOOK FORM ..............................................................

function sc_ebook_list( $atts , $content = null ) {
  extract(shortcode_atts(array(
    'style' => 'blue',
  ), $atts));

  return '<div class="m-ebook m-ebook--'. $style .'"><figure class="m-ebook-img"><img src="' . get_field('ebook_image','option') . '" alt="" /></figure>
  <div class="m-ebook-content">
      <h3>' . get_field('ebook_title','option') . '</h3>
      <p>' . get_field('ebook_text','option') . '</p>
      <div class="formwrap">' . get_field('ebook_form','option') . '</div>
    </div>

</div>';
}
add_shortcode( 'ebook-form', 'sc_ebook_list' );

# 7.4. END .....................................................................

# 7. END +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++