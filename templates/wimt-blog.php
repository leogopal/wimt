<?php
/*
 * Template Name: Blog
 * Description: Blog for WIMT with flexible content fields.
 */

?>
<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="mast">

      

      <!-- 2.1. BLOG INTRODUCTION - - - - - - - - - - - - - - - - - - - - -  -->

      <header class="section blog-header">

        <div class="container">

          <a href="<?php site_url(); ?>/blog">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/logo--interchange.svg" class="blog-header-logo" alt="Where is My Transport" />
          </a>

          <h5>By <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/site-logo-gray.svg" alt="Where is My Transport" /></h5>

        </div>

      </header>

      <!-- 2.1. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.2. LATEST POST - - - - - - - - - - - - - - - - - - - - - - - -  -->

        <div class="grid">

          <?php

            $args = array(
            'posts_per_page' => '1',
            );

            $query = new WP_query ( $args );
            if ( $query->have_posts() ) {

              while ( $query->have_posts() ) : $query->the_post();
                get_template_part('partials/teaser', 'blog-first');
              endwhile;

              wp_reset_postdata();

            };

          ?>

        </div>

      <!-- 2.2. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.3. SIGNUP BLOCK  - - - - - - - - - - - - - - - - - - - - - - -  -->

      <?php get_template_part('partials/component', 'mailing-list-weekly'); ?>

      <!-- 2.3. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.4. BLOG LISTINGS - - - - - - - - - - - - - - - - - - - - - - -  -->


      <!-- 2.2.2.1. Initialise the query -->

      <?php

      $post_type = get_field( 'post_type' );

      ?>

      <!-- 2.2.2. THE LOOP -->

      <?php foreach( $post_type as $key => $value){ ?>

        <!-- 2.2.2.1. Initialise the query -->

        <?php

        $search_term = $_GET['st'];
        $search_criteria = $_GET['type'];

        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $args = array (
          'post_type'              => $value,
          'paged'                  => $paged,
          'posts_per_page'         => '4',
          'ignore_sticky_posts'    => false,
          'order'                  => 'DESC',
          "s" => $search_term
        );

        $the_query = new WP_Query( $args );

        ?>

        <!-- 2.2.2.1. End -->

        <!-- 2.2.2.2. The loop -->

        <div class="section m-listings m-listings--<?php echo $value ?>">

          <div class="container">

            <header class="m-listings-header">

              <h3>

                Posts

              </h3>

              <form role="search" action="<?php the_permalink(); ?>" method="get" id="searchform"  class="search">
                <input type="text" name="st" />
                <!--<input type="hidden" name="post_type" value="jupiter_people" /> <!-- // hidden 'products' value -->
                <button type="submit"></button>
              </form>

              <?php
                if (function_exists(custom_pagination)) {
                  custom_pagination($the_query->max_num_pages,"",$paged);
                }
              ?>

            </header>

            <div class="grid -gutter">

          <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <?php get_template_part('partials/teaser', 'blog'); ?>

          <?php endwhile; ?>

          <?php wp_reset_postdata(); ?>

          <?php else:  ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php endif; ?>

            </div>

          </div>

        </div>

        <!-- 2.2.2.2. End -->

      <?php }; ?>

      <!-- 2.2.2.2. End -->
      
      <!-- 2.4. BLOG LISTINGS - - - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.5. BLOG LISTINGS - - - - - - - - - - - - - - - - - - - - - - -  -->

      <?php if(have_rows('blog_links')) { ?>

      <section class="blog-nav section">
      
        <div class="container">
        
          <div class="grid -gutter">

            <?php while ( have_rows('blog_links') ) : the_row(); ?>

              <div class="grid-item c-teaser">

                <h3><?php the_sub_field('link_title'); ?></h3>
                <p><?php the_sub_field('link_description'); ?></p>
                <a href="<?php the_sub_field('link_url'); ?>" class="btn -yellow">Read More</a>
              </div>    

            <?php endwhile; ?>

          </div>
        
        </div>
      
      </section>

      <?php } ?>

      <!-- 2.5. END - - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
