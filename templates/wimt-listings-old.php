<?php
/**
 * Template Name: Content Listings
 * Description: Used as a page template to show page contents, followed by a loop through a CPT archive
 */
?>

<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="mast">

      <!-- 2.1. ACF FIELDS - - - - - - - - - - - - - - - - - - - - - - - - - -->

      <?php

      // are there any rows within within our flexible content?
      if( have_rows('wimt_content') ):

        // loop through all the rows of flexible content
        while ( have_rows('wimt_content') ) : the_row();

        // PAGE HEADER
        if( get_row_layout() == 'block_header' )
          get_template_part('partials/component', 'header');

        // BLOCK LISTS
        if( get_row_layout() == 'block_content_list' )

          get_template_part('partials/component', 'list');

        // BLOCK GRID
        if( get_row_layout() == 'block_grid' )
          get_template_part('partials/component', 'blocks');

        endwhile; // close the loop of flexible content
      endif; // close flexible content conditional

      ?>

      <!-- 2.1. END - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -->

      <!-- 2.2. LISTINGS - - -  - - - - - - - - - - - - - - - - - - - - - -  -->

      <div class="container">

        <div class="newgrid -gutter">

          <div class="grid-item one-half">hello</div>
          <div class="grid-item one-half">hello</div>
          <div class="grid-item one-half">hello</div>
          <div class="grid-item one-half">hello</div>

        </div>

      </div>


      <!-- 2.2.1. VARIABLES -->

      <?php

      $post_type = get_field( 'post_type' );

      ?>

      <!-- 2.2.2. THE LOOP -->

      <?php foreach( $post_type as $key => $value){ ?>

        <!-- 2.2.2.1. Initialise the query -->

        <?php

        $search_term = $_GET['st'];
        $search_criteria = $_GET['type'];

        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $args = array (
          'post_type'              => $value,
          'paged'                  => $paged,
          'posts_per_page'         => '4',
          'ignore_sticky_posts'    => false,
          'order'                  => 'DESC',
          "s" => $search_term
        );

        $the_query = new WP_Query( $args );

        ?>

        <!-- 2.2.2.1. End -->

        <!-- 2.2.2.2. The loop -->

        <div class="section m-listings m-listings--<?php echo $value ?>">

          <div class="container">

            <header class="m-listings-header">

              <h3>

                <?php echo str_replace("_"," ",$value); ?>

              </h3>

              <form role="search" action="<?php the_permalink(); ?>" method="get" id="searchform"  class="search">
          	    <input type="text" name="st" />
          	    <!--<input type="hidden" name="post_type" value="jupiter_people" /> <!-- // hidden 'products' value -->
          	    <button type="submit"></button>
          	  </form>

              <?php if ($the_query->max_num_pages > 1) { // check if the max number of pages is greater than 1  ?>
                <nav class="m-listings-nav">
                  <ul class="pager">
                    <li><?php echo get_next_posts_link( '<', $the_query->max_num_pages ); ?></li>
                    <li><?php echo get_previous_posts_link( '>' );  ?></li>
                  </ul>
                </nav>
              <?php } ?>

            </header>

            <div class="grid -gutter">

          <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <?php get_template_part('partials/teaser', $value); ?>

          <?php endwhile; ?>

          <?php wp_reset_postdata(); ?>

          <?php else:  ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php endif; ?>

            </div>

          </div>

        </div>

        <!-- 2.2.2.2. End -->

      <?php }; ?>

      <!-- 2.2.2. END -->



      <!-- 2.2. END - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -->

      <!-- 2.3. FOOTER - - -  - - - - - - - - - - - - - - - - - - - - - - -  -->

      <?php

      // are there any rows within within our flexible content?
      if( have_rows('wimt_content') ):

        // loop through all the rows of flexible content
        while ( have_rows('wimt_content') ) : the_row();

        if( get_row_layout() == 'block_text' )

          get_template_part('partials/component', 'text');

        if( get_row_layout() == 'block_linked')

          get_template_part('partials/component', 'linked');

        // CONTACT GRID
        if( get_row_layout() == 'block_cta' )

          get_template_part('partials/component', 'cta');

        // MAILING LIST
        if( get_row_layout() == 'block_mailing_list' )

          get_template_part('partials/component', 'mailing-list');

        endwhile; // close the loop of flexible content
      endif; // close flexible content conditional

      ?>

      <!-- 2.4. END - - -  - - - - - - - - - - - - - - - - - - - - - - - - - -->

    </section>

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
