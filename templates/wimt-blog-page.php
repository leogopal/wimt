<?php
/*
 * Template Name: Blog Page
 * Description: Blog Page for WIMT with flexible content fields.
 */

?>
<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="mast">

      

      <!-- 2.1. BLOG INTRODUCTION - - - - - - - - - - - - - - - - - - - - -  -->

      <header class="section blog-header">

        <div class="container">

          <a href="<?php site_url(); ?>/blog">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/logo--interchange.svg" class="blog-header-logo" alt="Where is My Transport" />
          </a>

          <h5>By <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/site-logo-gray.svg" alt="Where is My Transport" /></h5>

        </div>

      </header>

      <!-- 2.1. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->


      <!-- 2.1. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <div class="section blog-intro">

        <div class="container">

          <div class="blog-intro-content is-open -open">

            <?php the_field('blog_text'); ?>

            <?php if (have_rows('blog_contributors')) {?>
              <h4>Contributors</h4>
            <?php } ?>

            <ul>

            <?php while ( have_rows('blog_contributors') ) : the_row(); ?>

              <li>

                <img src="<?php the_sub_field('blog_contributor_photo'); ?>" />
                <div class="contents">
                  <p><?php the_sub_field('blog_contributor_bio'); ?></p>
                </div>
              </li>

            <?php endwhile; ?>

            </ul>

          </div>

        </div>

      </div>

      <!-- 2.1. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->


      <!-- 2.3. SIGNUP BLOCK  - - - - - - - - - - - - - - - - - - - - - - -  -->

      <?php get_template_part('partials/component', 'mailing-list-weekly'); ?>

      <!-- 2.3. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.4. LATEST LISTINGS  - - - - - - - - - - - - - - - - - - - - - - -->

      <div class="section">

        <div class="container">

          <div class="grid">

            <?php

            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $args = array (
              'post_type'              => 'post',
              'paged'                  => $paged,
              'posts_per_page'         => '2',
              'ignore_sticky_posts'    => false,
              'order'                  => 'DESC',
              "s" => $search_term
            );

            $the_query = new WP_Query( $args );

            ?>

              <?php

                $args = array(
                'posts_per_page' => '2',
                );

                $query = new WP_query ( $args );
                if ( $query->have_posts() ) { ?>

                  <div class="grid -gutter">

                  <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                    <?php get_template_part('partials/teaser', 'blog'); ?>

                  <?php endwhile; ?>

                  <?php wp_reset_postdata(); ?>

                  <?php else:  ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
                  <?php endif; ?>

                    </div>
                    <?php
                  wp_reset_postdata();

                };

              ?>

            </div>

          </div>
        
        </div>

      <!-- 2.4. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

    </section>

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
