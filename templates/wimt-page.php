<?php
/*
 * Template Name: Flexible Page
 * Description: Page template for WIMT with flexible content fields
 */

?>

<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="mast">

      <?php // open the WordPress loop
        if (have_posts()) : while (have_posts()) : the_post();
      ?>

        <article id="p-<?php the_ID(); ?>" class="m-page" <?php post_class(); ?>>

            <?php

            // are there any rows within within our flexible content?
            if( have_rows('wimt_content') ):

              // loop through all the rows of flexible content
              while ( have_rows('wimt_content') ) : the_row();

              // PAGE HEADER
              if( get_row_layout() == 'block_header' )
                get_template_part('partials/component', 'header');

              // PAGE HEADER
              if( get_row_layout() == 'block_quote' )
                get_template_part('partials/component', 'quote');

              // BLOCK LISTS
              if( get_row_layout() == 'block_text' )

                get_template_part('partials/component', 'text');

              // BLOCK LISTS
              if( get_row_layout() == 'block_content_list' )

                get_template_part('partials/component', 'list');

              // BLOCK GRID
              if( get_row_layout() == 'block_content_grid' )
                get_template_part('partials/component', 'blocks');

              // CONTACT GRID
              if( get_row_layout() == 'block_cta' )

                get_template_part('partials/component', 'cta');

              // CONTACT GRID
              if( get_row_layout() == 'block_contact' )

                get_template_part('partials/component', 'contact');

              // MAILING LIST
              if( get_row_layout() == 'block_mailing_list' )

                get_template_part('partials/component', 'mailing-list');

              // MAILING LIST
              if( get_row_layout() == 'block_linked')

                get_template_part('partials/component', 'linked');

              // MAILING LIST
              if( get_row_layout() == 'block_founders')

                get_template_part('partials/component', 'founders');

              if( get_row_layout() == 'block_testimonials')

                get_template_part('partials/component', 'testimonials');

              // MAILING LIST
              if( get_row_layout() == 'block_team')

                get_template_part('partials/component', 'team');

              // MAILING LIST
              if( get_row_layout() == 'block_gallery')

                get_template_part('partials/component', 'gallery');


              endwhile; // close the loop of flexible content
            endif; // close flexible content conditional

            ?>

        </article>
      <?php
        endwhile; endif; // close the WordPress loop
      ?>

    </section>

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
