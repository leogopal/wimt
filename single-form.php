<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/production/stylesheet.css" media="screen" type="text/css" />
  <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/production/jquery.fullpage.min.css" media="screen" type="text/css" />

  

</head>

<body <?php body_class(); ?>>

  <div class="wrapper">

    <!-- 1. MASTHEAD +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <header class="section masthead -has-header">

      <div class="container">

        <a href="<?php echo site_url(); ?>" class="masthead-logo">
          <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/logo.svg" class="" alt="Where is My Transport" />
        </a>

        <a href="#main-navigation" class="reveal masthead-burger">
          <img src="<?php bloginfo('template_directory'); ?>/assets/images/icons/icon--burger.svg" class="burger-dark" alt="Open the menu" />
          <img src="<?php bloginfo('template_directory'); ?>/assets/images/icons/icon--burger-white.svg" class="burger-light" alt="Open the menu" />
        </a>

        <nav class="masthead-nav" id="main-navigation">

          <header class="masthead-nav-header">
            <a href="#main-navigation" class="reveal"><img src="<?php bloginfo('template_directory'); ?>/assets/images/icons/icon--close.svg" class="c-icon" alt="Close menu" /></a>
            <p>Menu</p>

          </header>

          <ul id="menu-main-navigation" class="menu">
            <li><a href="<?php echo site_url(); ?>">Back to website</a></li>
            
            <li><a href="https://identity.whereismytransport.com/login">Log In</a></li>
            </ul>

        </nav>

      </div>

    </header>

    <!-- 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="mast">

      <!-- 2.1. BLOG INTRODUCTION - - - - - - - - - - - - - - - - - - - - -  -->

      <div class="single-form-content">

        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();

        ?>

        <h1><?php the_title(); ?></h1>

        <?php the_field('forms_text'); ?>

        <?php the_field('forms_code'); ?>

        <?php

          // End of the loop.
        endwhile;
        ?>

      </div>

      <!-- 2.1. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

    </section>

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

  </div>

 <?php wp_footer(); ?>

</body>
</html>
