<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
    <?php gravity_form_enqueue_scripts( 1, true ); ?>
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/assets/production/stylesheet.css"
          media="screen" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/assets/production/jquery.fullpage.min.css"
          media="screen" type="text/css"/>
    <link rel="stylesheet" href="<?php bloginfo( 'template_directory' ); ?>/assets/production/custom.css" media="screen"
          type="text/css"/>

</head>

<body <?php body_class(); ?>>


<div class="wrapper">

	<?php
	if ( have_rows( 'wimt_content' ) ):
		while ( have_rows( 'wimt_content' ) ) : the_row();

			if ( get_row_layout() == 'block_header' ) {
				$has_header = '-has-header';
			}

		endwhile; // close the loop of flexible content
	endif; // close flexible content conditional
	?>

    <!-- 1. MASTHEAD +++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
    <header class="section masthead <?php echo $has_header ?>">

        <div class="container">

            <a href="<?php echo site_url(); ?>" class="masthead-logo">
                <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/site/logo.svg" class=""
                     alt="Where is My Transport"/>
            </a>

            <a href="#main-navigation" class="reveal masthead-burger">
                <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/icons/icon--burger.svg"
                     class="burger-dark" alt="Open the menu"/>
                <img src="<?php bloginfo( 'template_directory' ); ?>/assets/images/icons/icon--burger-white.svg"
                     class="burger-light" alt="Open the menu"/>
            </a>

            <nav class="masthead-nav" id="main-navigation">

                <header class="masthead-nav-header">
                    <a href="#main-navigation" class="reveal"><img
                                src="<?php bloginfo( 'template_directory' ); ?>/assets/images/icons/icon--close.svg"
                                class="c-icon" alt="Close menu"/></a>
                    <p>Menu</p>

                </header>

				<?php wp_nav_menu(
					array(
						'theme_location' => 'primary-nav',
						'container'      => false
					)
				); ?>
            </nav>

        </div>

    </header>

    <!-- 1. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
