<?php

# 3.1. CASE STUDIES ------------------------------------------------------------

# 3.1.1 CONTENT TYPE

function wimt_ct_case_study() {
  $labels = array(
    'name'               => _x( 'Case Studies', 'post type general name' ),
    'singular_name'      => _x( 'Case Study', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Case Study' ),
    'edit_item'          => __( 'Edit Case Study' ),
    'new_item'           => __( 'New Case Study' ),
    'all_items'          => __( 'All Case Studies' ),
    'view_item'          => __( 'View Case Study' ),
    'search_items'       => __( 'Search Case Studies' ),
    'not_found'          => __( 'No case studies found' ),
    'not_found_in_trash' => __( 'No case studies found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Case Studies',

  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our case studies and case study specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => false,
    'query_var'     => false,
    'rewrite'       => array(
      'slug' => 'case-studies',
      'with_front' => false
    )

  );
  register_post_type( 'case_study', $args );
}
add_action( 'init', 'wimt_ct_case_study' );

# 3.1.1. END

# 3.1.2. CASE STUDY CATEGORIES

function wimt_ct_case_study_cat() {
  $labels = array(
    'name'              => _x( 'Case Study Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Case Study Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Categories' ),
    'all_items'         => __( 'All Categories' ),
    'parent_item'       => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item'         => __( 'Edit Category' ),
    'update_item'       => __( 'Update Category' ),
    'add_new_item'      => __( 'Add New Category' ),
    'new_item_name'     => __( 'New Category' ),
    'menu_name'         => __( 'Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'case_study_category', 'case_study', $args );
}
add_action( 'init', 'wimt_ct_case_study_cat', 0 );

# 3.1.2. END

# 3.1.3. CASE STUDY CATEGORIES

function wimt_ct_case_study_client() {
  $labels = array(
    'name'              => _x( 'Case Study Clients', 'taxonomy general name' ),
    'singular_name'     => _x( 'Case Study Client', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Clients' ),
    'all_items'         => __( 'All Clients' ),
    'parent_item'       => __( 'Parent Client' ),
    'parent_item_colon' => __( 'Parent Client:' ),
    'edit_item'         => __( 'Edit Client' ),
    'update_item'       => __( 'Update Client' ),
    'add_new_item'      => __( 'Add New Client' ),
    'new_item_name'     => __( 'New Client' ),
    'menu_name'         => __( 'Clients' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'case_study_client', 'case_study', $args );
}
add_action( 'init', 'wimt_ct_case_study_client', 0 );

# 3.1.3. END

# 3.1. END ---------------------------------------------------------------------

# 3.2. AWARDS ------------------------------------------------------------------

# 3.2.1 CONTENT TYPE

function wimt_ct_awards() {
  $labels = array(
    'name'               => _x( 'Awards', 'post type general name' ),
    'singular_name'      => _x( 'Award', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Award' ),
    'edit_item'          => __( 'Edit Award' ),
    'new_item'           => __( 'New Award' ),
    'all_items'          => __( 'All Awards' ),
    'view_item'          => __( 'View Award' ),
    'search_items'       => __( 'Search Awards' ),
    'not_found'          => __( 'No awards found' ),
    'not_found_in_trash' => __( 'No awards found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Awards'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our awards and award specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => false,
    'query_var'     => true,
    'rewrite'             => array( 'slug' => 'awards' ),

  );
  register_post_type( 'award', $args );
}
add_action( 'init', 'wimt_ct_awards' );

# 3.2.1. END

# 3.1.2. AWARDS CATEGORIES

function wimt_ct_award_cat() {
  $labels = array(
    'name'              => _x( 'Award Categories', 'taxonomy general name' ),
    'singular_name'     => _x( 'Award Category', 'taxonomy singular name' ),
    'search_items'      => __( 'Search Categories' ),
    'all_items'         => __( 'All Categories' ),
    'parent_item'       => __( 'Parent Category' ),
    'parent_item_colon' => __( 'Parent Category:' ),
    'edit_item'         => __( 'Edit Category' ),
    'update_item'       => __( 'Update Category' ),
    'add_new_item'      => __( 'Add New Category' ),
    'new_item_name'     => __( 'New Category' ),
    'menu_name'         => __( 'Categories' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'award_category', 'award', $args );
}
add_action( 'init', 'wimt_ct_award_cat', 0 );

# 3.1.2. END

# 3.2. END ---------------------------------------------------------------------

# 3.3. NEWS --------------------------------------------------------------------

# 3.3.1 CONTENT TYPE

function wimt_ct_news() {
  $labels = array(
    'name'               => _x( 'News', 'post type general name' ),
    'singular_name'      => _x( 'News', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add News' ),
    'edit_item'          => __( 'Edit News' ),
    'new_item'           => __( 'New News' ),
    'all_items'          => __( 'All News' ),
    'view_item'          => __( 'View News' ),
    'search_items'       => __( 'Search News' ),
    'not_found'          => __( 'No News found' ),
    'not_found_in_trash' => __( 'No News found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'News'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our News and News specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => true,
    'query_var'     => true,
    'rewrite'             => array( 'slug' => 'news' ),

  );
  register_post_type( 'news', $args );
}
add_action( 'init', 'wimt_ct_news' );

# 3.3.1. END

# 3.3. END ---------------------------------------------------------------------

# 3.4. PRESS RELEASE -----------------------------------------------------------

# 3.4.1 CONTENT TYPE

function wimt_ct_press() {
  $labels = array(
    'name'               => _x( 'Press Releases', 'post type general name' ),
    'singular_name'      => _x( 'Press Release', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Press Release' ),
    'edit_item'          => __( 'Edit Press Release' ),
    'new_item'           => __( 'New Press Release' ),
    'all_items'          => __( 'All Press Releases' ),
    'view_item'          => __( 'View Press Release' ),
    'search_items'       => __( 'Search Press Releases' ),
    'not_found'          => __( 'No press releases found' ),
    'not_found_in_trash' => __( 'No press releases found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Press Releases'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our press releases and press specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => true,
    'query_var'     => true,
    'rewrite'             => array( 'slug' => 'press' ),

  );
  register_post_type( 'press', $args );
}
add_action( 'init', 'wimt_ct_press' );

# 3.4.1. END

# 3.4. END ---------------------------------------------------------------------

# 3.5. GENERAL FORMS -----------------------------------------------------------

# 3.5.1 CONTENT TYPE

function wimt_forms() {
  $labels = array(
    'name'               => _x( 'Forms', 'post type general name' ),
    'singular_name'      => _x( 'Form', 'post type singular name' ),
    'add_new'            => _x( 'Add New', 'book' ),
    'add_new_item'       => __( 'Add New Form' ),
    'edit_item'          => __( 'Edit Form' ),
    'new_item'           => __( 'New Form' ),
    'all_items'          => __( 'All Forms' ),
    'view_item'          => __( 'View Form' ),
    'search_items'       => __( 'Search Forms' ),
    'not_found'          => __( 'No Forms found' ),
    'not_found_in_trash' => __( 'No Forms found in the Trash' ),
    'parent_item_colon'  => '',
    'menu_name'          => 'Forms',

  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Holds our Forms and Form specific data',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => true,
    'query_var'     => false,
    'rewrite'       => array(
      'slug' => 'form',
      'with_front' => false
    )

  );
  register_post_type( 'form', $args );
}
add_action( 'init', 'wimt_forms' );

# 3.5.1. END

# 3.5. CASE STUDIES ------------------------------------------------------------