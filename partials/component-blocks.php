<div class="m-blocks">

  <?php while ( have_rows('item') ) : the_row(); ?>
  <div class="grid -<?php the_sub_field('item-orientation') ?>">

    <div class="m-blocks-details">

      <h3><?php the_sub_field('item_title') ?></h3>

      <p><?php the_sub_field('item_text') ?></p>

      <?php if( get_sub_field('add_button') == 'yes') { ?>
      <a href="<?php the_sub_field('button_url') ?>" class="btn -yellow -inline"><?php the_sub_field('button_text') ?></a>
      <?php } ?>

    </div>

    <figure class="m-blocks-image">

      <img src="<?php the_sub_field('item_image') ?>" alt="" class="" />

    </figure>

  </div>

  <?php endwhile; ?>

</div>
