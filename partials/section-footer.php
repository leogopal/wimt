<footer class="section footend">

  <div class="container">

    <div class="grid">

      <!-- 4.1. FOOTER DETAILS - - - - - - - - - - - - - - - - - - - - - - -->

      <div class="grid-item footend-details">

        <a href="">
          <img src="<?php the_field('global_logo', 'option'); ?>" />
        </a>

        <ul>
          <li>
            <a href="mailto:<?php the_field('global_email', 'option'); ?>">
              <?php the_field('global_email', 'option'); ?>
            </a>
          </li>
          <li>
            <a href="tel:<?php the_field('global_phone', 'option'); ?>">
              <?php the_field('global_phone', 'option'); ?>
            </a>
          </li>
        </ul>

      </div>

      <!-- 4.1. END - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 4.2. FOOTER NAV: 1 - - - - - - - - - - - - - - - - - - - - - -  -->


      <nav class="grid-item footend-nav">
        <h4>Learn more</h4>
        <?php if ( has_nav_menu( 'footer-nav-1' ) ) { ?>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-1' ) ); ?>
        <?php } ?>
      </nav>

      <!-- 4.2. END - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 4.3. FOOTER NAV: 2 - - - - - - - - - - - - - - - - - - - - - -  -->

      <nav class="grid-item footend-nav">
        <h4>Company</h4>
        <?php if ( has_nav_menu( 'footer-nav-2' ) ) { ?>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-2' ) ); ?>
        <?php } ?>
      </nav>

      <!-- 4.3. END - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 4.4. FOOTER NAV: 3 - - - - - - - - - - - - - - - - - - - - - -  -->

      <nav class="grid-item footend-nav">
        <h4>Resources</h4>
        <?php if ( has_nav_menu( 'footer-nav-3' ) ) { ?>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-3' ) ); ?>
        <?php } ?>
      </nav>

      <!-- 4.4. END - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

    </div>

    <div class="grid">

      <!-- 4.5. FOOTER NAV: SOCIAL - - - - - - - - - - - - - - - - - - - - -->

      <nav class="grid-item footend-nav -bottom">
        <ul>
          <?php if ( get_field('global_social_facebook', 'option') ) {  ?>
          <li>
            <a href="https://facebook.com/<?php the_field('global_social_facebook', 'option'); ?>" target="_blank">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/icons/icon--social-fb.svg" class="c-icon -small" />
            </a>
          </li>
          <?php };  ?>
          <?php if ( get_field('global_social_twitter', 'option') ) {  ?>
          <li>
            <a href="https://twitter.com/<?php the_field('global_social_twitter', 'option'); ?>" target="_blank">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/icons/icon--social-twitter.svg" class="c-icon -small" />
            </a>
          </li>
          <?php };  ?>
          <?php if ( get_field('global_social_linkedin', 'option') ) {  ?>
          <li>
            <a href="https://linkedin.com/company/<?php the_field('global_social_linkedin', 'option'); ?>" target="_blank">
              <img src="<?php bloginfo('template_directory'); ?>/assets/images/icons/icon--social-linkedin.svg" class="c-icon -small" />
            </a>
          </li>
          <?php };  ?>
        </ul>
      </nav>

      <!-- 4.5. END - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 4.5. FOOTER NAV: SOCIAL - - - - - - - - - - - - - - - - - - - - -->


      <div class="grid-item footend-nav -bottom -legal">
        <h6>&copy; <?php echo date('Y'); ?> - WhereIsMyTransport Ltd.</h6>
        <?php if ( has_nav_menu( 'footer-nav-4' ) ) { ?>
          <?php wp_nav_menu( array( 'theme_location' => 'footer-nav-4' ) ); ?>
        <?php } ?>
      </div>

      <!-- 4.5. END - - - - - - - - - - - - - - - - - - - - - - - - - - -  -->

    </div>

  </div>

</footer>
