<ul id="post-<?php the_ID(); ?>" <?php post_class('c-teaser m-teaser-press'); ?>>
  <li>
    <span><?php the_time(get_option('date_format')); ?></span>
    <span><?php the_title(); ?></span>
  </li>
  <li>
    <a href="<?php the_field('preview_text'); ?>" target="_blank">
      Preview Text
    </a>
  </li>
  <li>
    <a href="<?php the_field('preview_images'); ?>" target="_blank">
      Preview Images
    </a>
  </li>
  <li>
    <a href="<?php the_field('download_link'); ?>" target="_blank">
      Download
    </a>
  </li>
</ul>
