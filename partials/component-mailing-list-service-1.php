<div class="section form m-signup -quarterly">

  <div class="container">

    <header class="form-header">
      <p>Sign up to our quarterly Cities and Governments newsletter for company and public sector news.</p>
    </header>

    <div class="m-signup-form">
	    <?php gravity_form( 5, false, false, false, '', true, 12 ); ?>
    </div>

  </div>

</div>
