
<div class="award-date"><span><?php the_field('award_date'); ?></span></div>
<div class="award-title"><?php the_title(); ?></div>
<div class="award-result"><?php the_field('award_result'); ?></div>
