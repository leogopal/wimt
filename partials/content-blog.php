<article id="post-<?php the_ID(); ?>" <?php post_class('section c-content'); ?>>

  <div class="container">

    <header class="c-content-header">

      <ul>
        <li><strong><?php the_time(get_option('date_format')); ?></strong></li>
        <li>
          |&nbsp;&nbsp;&nbsp;by 
          <?php 

            if ( function_exists( 'coauthors' ) ) {
              coauthors();
          } else {
              the_author();
          }
          
          ?>
        </li>
      </ul>

      <h1><?php the_title(); ?></h1>

    </header>

    <figure class="c-content-img">
      <img src="<?php the_field('post_image'); ?>" alt="<?php the_title(); ?>" />

      <?php if(get_field('post_caption')) { ?> 

      <figcaption>
        <?php the_field('post_caption'); ?>
      </figcaption>

      <?php } ?>

    </figure>

    <?php the_content(); ?>

    <footer class="c-content-footer">
      <ul>
        <li>
          <?php

          $customAddThis = array(
            'size' => '32', // size of the icons.  Either 16 or 32
            'services' => 'facebook,twitter,linkedin', // the services you want to always appear
            'preferred' => '3', // the number of auto personalized services
            'more' => false // if you want to have a more button at the end
          );

          do_action(
          'addthis_widget',
          get_permalink(),
          get_the_title(),
          'large_toolbox'
          );

          ?>
        </li>
        <li><?php the_tags('', '', ''); ?></li>
      </ul>

    </footer><!-- .entry-footer -->

    <aside class="c-content-nav">

      <?php

      $previous = get_previous_post(true);
      $previous_link  = get_permalink($previous->ID);
      $previous_title = $previous->post_title;

      $next = get_next_post();
      $next_link  = get_permalink($next->ID);
      $next_title = $next->post_title;

      ?>

      <ul>
        <?php if( get_previous_post_link() ) { ?>
          <li>
            <a href="<?php echo $previous_link; ?>" class="nextprev">
              <span>Previous </span>
              <strong><?php echo $previous_title; ?></strong>
            </a>
          </li>
        <?php } ?>
        <?php if( get_next_post_link() ) { ?>
          <li>
            <a href="<?php echo $next_link; ?>" class="nextprev">
              <span>Next </span>
              <strong><?php echo $next_title; ?></strong>
            </a>
          </li>
        <?php } ?>
      </ul>

    </aside>

  </div>

</article>
