<?php

  if ( get_sub_field('hidden_by_default') == 1 ) {
    $isHidden = 'c-gallery--hidden';
  }

?>

<div class="c-gallery <?php echo $isHidden ?>" id="<?php the_sub_field('custom_class'); ?>">

  <!-- 1. TITLE -->

  <h2 class="section"><?php the_sub_field('title') ?></h2>

  <!-- 1. END -->

  <!-- 2. GRID -->

  <ul class="grid">

    <?php

      $event_holder = array();
      $person_holder = array();

      //This will be your while....
      while ( have_rows('images') ) : the_row();
        if (get_sub_field('image_type') == 'event') {
          $event_holder[] = array(
            'type' => get_sub_field('image_type'),
            'image' => get_sub_field('image'),
            'description' => get_sub_field('description'),
          );
        } else {
          $person_holder[] = array(
            'type' => get_sub_field('image_type'),
            'image' => get_sub_field('image'),
            'name' => get_sub_field('name'),
            'designation' => get_sub_field('designation')
          );
        }
      endwhile;

      $bShowBanner = true;
      $event_right = false;
      foreach ($event_holder as $e) { ?>

        <?php if (!$event_right) { ?>

        <li class="grid-item">
          <img src="<?php echo $e['image']; ?>" alt="<?php echo $e['description']; ?>" />
          <div>
            <span><?php echo $e['description']; ?></span>
          </div>
        </li>

        <?php } ?>

      <?php $total = 4;
      if (count($person_holder) < 4) {
        $total = count($person_holder);
      }?>

      <li class="grid-item">

        <ul class="grid">

      <?php for ($i = 0; $i < $total; $i++) { ?>

      <li class="grid-item">
        <img src="<?php echo $person_holder[0]['image']; ?>" alt="<?php echo $value['name']; ?>" />
        <div>
          <span><?php echo $person_holder[0]['name']; ?></span>
          <em><?php echo $person_holder[0]['designation']; ?></em>
        </div>
      </li>

      <?php  unset($person_holder[0]);
        $person_holder = array_values($person_holder);
      } ?>

      </ul>
      </li>

      <?php if ($event_right) { ?>

      <li class="grid-item">
        <img src="<?php echo $e['image']; ?>" alt="<?php echo $e['description']; ?>" />
        <div>
          <span><?php echo $e['description']; ?></span>
        </div>
      </li>

      <?php } ?>

      <?php
        if ($event_right) {
          $event_right = false;
        } else {
          $event_right = true;
        }
      ?>


      <?php if (get_sub_field('hiring') == '1' && $bShowBanner) { ?>
        <li class="grid-item hiring">
          <?php the_sub_field('hiring_text'); ?>
        </li>
      <?php  $bShowBanner = false;
        }
      }

      $counter = 0;

      if (count($person_holder) > 0) {

        $totalLoop = count($person_holder);   //We need to know the total amount

        for($i = 0; $i < $totalLoop; $i++) {

          if ($counter == 0) { ?>

            <li class="grid-item">

              <ul class="grid">

          <?php } //end of id L93 ?>

          <li class="grid-item">
            <img src="<?php echo $person_holder[0]['image']; ?>" alt="<?php echo $value['name']; ?>" />
            <div>
              <span><?php echo $person_holder[0]['name']; ?></span>
              <em><?php echo $person_holder[0]['designation']; ?></em>
            </div>
          </li>

          <?php
            unset($person_holder[0]);
            $person_holder = array_values($person_holder);


          if ($counter == 3) {?>

            </li>

          </ul>

            <?php $counter = 0;
          } else {
            $counter++;
          } //end of if L114
        } // end of for L91
      }// end of if L89

      if ($counter != 0) { ?>
        </ul>
        </li>


      <?php };
    ?>

  </ul>

  <!-- 2. END -->


</div>
