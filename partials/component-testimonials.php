<aside class="c-linked c-linked-testimonials">

  <ul class="c-carousely">

    <?php while ( have_rows('testimonial') ) : the_row(); ?>

      <li>
        <img src="<?php the_sub_field('background') ?>" />

        <div class="c-carousely-item">

          <strong>TESTIMONIAL</strong>

          <?php the_sub_field('text') ?>
          <strong><?php the_sub_field('name') ?></strong>
          <small><?php the_sub_field('designation') ?></small>
          <img src="<?php the_sub_field('logo') ?>" />

          <aside>
            <span><a href="#" class="previous"><img src="<?php bloginfo('template_directory'); ?>/assets/images/site/icon--arrow-left.svg" class="c-icon " /></a></span>
            <span><a href="#" class="next"><img src="<?php bloginfo('template_directory'); ?>/assets/images/site/icon--arrow-right.svg" class="c-icon " /></a></span>
          </aside>

        </div>

      </li>

    <?php endwhile; ?>

  </ul>

  <ul class="c-carousely-thumbs">
    <?php while ( have_rows('testimonial') ) : the_row(); ?>

      <li>
        <img src="<?php the_sub_field('logo') ?>" />
      </li>

    <?php endwhile; ?>
  </ul>

</aside>
