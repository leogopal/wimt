<header class="section -large m-page-header" style="background-image: url('<?php the_sub_field('header_background') ?>');">

  <div class="container">

    <?php the_title( '<h4>', '</h4>' ); ?>

    <?php if( get_sub_field('media_header') == 1 ) {?>

    <div class="m-page-header-content">

      <div class="">
        <h2><?php the_sub_field('header_lead') ?></h2>
        <p><?php the_sub_field('header_text') ?></p>
      </div>
      <figure class="m-page-header-video">
        <a href="#modal-media" class="reveal">
          <img src="<?php the_sub_field('media_cover') ?>" />
        </a>
        <span><?php the_sub_field('media_text') ?></span>
      </figure>

    </div>


    <?php } else { ?>


    <h2><?php the_sub_field('header_lead') ?></h2>
    <p><?php the_sub_field('header_text') ?></p>

    <?php if ( get_sub_field('add_button') == 'yes' ) { ?>

      <a href="<?php the_sub_field('button_url') ?>" class="reveal btn -blue "><?php the_sub_field('button_text') ?></a>

    <?php } ?>

  <?php } ?>

  </div>

</header>

<aside class="modal" id="modal-media">

  <div class="modal-body">

    <a href="#modal-media" class="reveal modal-closer">

    </a>

    <?php the_sub_field('media') ?>

  </div>

</aside>
