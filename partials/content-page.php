<article id="post-<?php the_ID(); ?>" <?php post_class('section c-content'); ?>>

  <div class="container">

    <header class="c-content-header">

      <ul>
        <li><strong><?php the_time(get_option('date_format')); ?></strong></li>
        <li>|&nbsp;&nbsp;&nbsp;by <?php the_author(); ?></li>
      </ul>

      <h1><?php the_title(); ?></h1>

    </header>

    <figure class="c-content-img">
      <img src="<?php the_field('post_image'); ?>" alt="<?php the_title(); ?>" />
    </figure>

    <?php the_content(); ?>

    <footer class="c-content-footer">
      <ul>
        <li>
          <?php

          $customAddThis = array(
            'size' => '32', // size of the icons.  Either 16 or 32
            'services' => 'facebook,twitter,linkedin', // the services you want to always appear
            'preferred' => '3', // the number of auto personalized services
            'more' => false // if you want to have a more button at the end
          );

          do_action(
          'addthis_widget',
          get_permalink(),
          get_the_title(),
          'large_toolbox'
          );

          ?>
        </li>
        <li><?php the_tags(); ?></li>
      <ul>

    </footer><!-- .entry-footer -->

    <aside class="c-content-nav">

      <ul>
        <?php if( get_previous_post_link() ) { ?>
        <li>
          <span>Previous</span>
          <?php previous_post_link('<strong>%link</strong>'); ?>
        </li>
        <?php } ?>
        <?php if( get_next_post_link() ) { ?>
        <li>
          <span>Next</span>
          <?php next_post_link('<strong>%link</strong>'); ?>
        </li>
        <?php } ?>
      </ul>

    </aside>

  </div>

</article>
