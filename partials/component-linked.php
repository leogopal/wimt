<?php

$post_type = get_sub_field('content_type');
$post_count = get_sub_field('post_count');

if ($post_count == '-1') {

  $args = array (
    'post_type'              => array( $post_type ),
    'nopaging'               => true,
    'ignore_sticky_posts'    => false,
    'order'                  => 'DESC',
  );

} else {

  $args = array (
    'post_type'              => array( $post_type ),
    'nopaging'               => false,
    'posts_per_page'         => $post_count,
    'ignore_sticky_posts'    => false,
    'order'                  => 'DESC',
  );

};

?>

<aside class="c-linked  c-linked-<?php the_sub_field('content_type') ?>">

  <div class="c-carousel">



<?php


  // The Query
  $query = new WP_Query( $args );

  // The Loop
  if ( $query->have_posts() ) {

    while ( $query->have_posts() ) {

      $query->the_post();



      echo '<div class="c-carousel-item">';

      get_template_part('partials/component', $post_type);

      echo '</div>';

    }

  } else {
    // no posts found
  }

  // Restore original Post Data
  wp_reset_postdata();

  ?>

  </div>

</aside>
