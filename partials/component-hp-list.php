<div class="c-panel <?php the_sub_field('layout') ?>" >

  <div class="container">

    <?php if ( get_sub_field('list_title') ) { ?>
      <h3><span><?php the_sub_field('list_title') ?></span></h3>
    <?php  }; ?>

    <?php while ( have_rows('list') ) : the_row(); ?>

      <div class="c-slide  -<?php the_sub_field('item-orientation') ?>">

        <?php if( get_sub_field('list_icon')): ?>
          <figure class="c-list-img">
            <img src="<?php the_sub_field('list_icon') ?>" />
          </figure>
        <?php endif; ?>

        <div class="c-list-content">

          <?php if( get_sub_field('list_text')): ?>
            <p><?php the_sub_field('list_text') ?></p>
          <?php endif; ?>

        </div>

      </div>

    <?php endwhile; ?>

  </div>

</div>
