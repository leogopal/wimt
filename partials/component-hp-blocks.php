<?php if ( get_sub_field('homepage') == 'horizontal' ) { ?>

  <div class="c-panel " >

    <div class="m-blocks <?php the_sub_field('homepage') ?>">

      <?php if ( get_sub_field('list_title') ) { ?>
        <h3><span><?php the_sub_field('list_title') ?></span></h3>
      <?php  }; ?>

      <?php while ( have_rows('item') ) : the_row(); ?>

        <div class="c-slide">

            <div class="m-blocks-details">

              <p><?php the_sub_field('item_text') ?></p>

              <a href="<?php the_sub_field('button_url') ?>" class="btn -yellow -inline"><?php the_sub_field('button_text') ?></a>

            </div>

            <figure class="m-blocks-image <?php the_sub_field('custom_class') ?>">

              <img src="<?php the_sub_field('item_image') ?>" alt="" />

            </figure>

        </div>

      <?php endwhile; ?>

    </div>

  </div>

<?php } else { ?>

  <?php while ( have_rows('item') ) : the_row(); ?>

    <div class="c-panel slide-grid" >

      <div class="m-blocks-details">

        <p><?php the_sub_field('item_text') ?></p>

      </div>

      <figure class="m-blocks-image <?php the_sub_field('custom_class') ?>">

        <img src="<?php the_sub_field('item_image') ?>" alt="" />

      </figure>

    </div>

  <?php endwhile; ?>

<?php } ?>
