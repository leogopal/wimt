<article id="post-<?php the_ID(); ?>" <?php post_class('grid-item c-teaser m-teaser-case_study'); ?>>

  <a href="<?php the_permalink(); ?>">

  <header class="c-teaser-header">
    <?php
    $clients = wp_get_post_terms( $post->ID, 'case_study_client', array("fields" => "names") );
    $cat = wp_get_post_terms( $post->ID, 'case_study_category', array("fields" => "names") );

 ?>
    <p><em><?php echo implode(', ',$clients); ?></em></p>

    <h2>

        <?php echo substr(the_title('', '', FALSE), 0, 74); ?>...

    </h2>
  </header>

  <figure class="c-teaser-img">
    <a href="<?php the_permalink() ?>">
      <img src="<?php the_field('post_image'); ?>" alt="<?php the_title(); ?>" />
    </a>
  </figure>

  <footer class="c-teaser-footer">
    <h3><?php echo implode(', ',$cat); ?></h3>
  </footer><!-- .entry-footer -->

  </a>

</article>
