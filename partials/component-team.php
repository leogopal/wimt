<div class="section m-team">

  <div class="container">

    <h3><?php the_sub_field('title') ?></h3>

    <ul class="m-team-list">

      <?php while ( have_rows('photos') ) : the_row(); ?>
      <li>

        <img src="<?php the_sub_field('photo') ?>" alt="" class="" />

      </li>

      <?php endwhile; ?>

    </ul>

    <?php the_sub_field('text') ?>

  </div>

</div>
