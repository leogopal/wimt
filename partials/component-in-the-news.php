<div class="section c-quote">

  <div class="container">

    <p>In The News</p>

    <?php $l_counter = 0; ?>

    <ul class="grid -gutter">
      <?php while ( have_rows('news_snippet') ) : the_row(); ?>
        <?php $l_counter ++; ?>
      <li class="grid-item">
        <a href="#quote-<?php echo $l_counter; ?>" class="reveal-quotes"><img src="<?php the_sub_field('news_logo') ?>" alt="" class="" /></a>
      </li>
      <?php endwhile; ?>
    </ul>

    <?php $q_counter = 0; ?>

    <?php while ( have_rows('news_snippet') ) : the_row(); ?>
      <?php $q_counter ++; ?>
    <p class="quotes" id="quote-<?php echo $q_counter; ?>"><em>“<?php the_sub_field('news_quote') ?>”</em> <strong>– <?php the_sub_field('news_publication') ?></strong>

      <?php if (get_sub_field('news_url')) { ?>
        <a href="<?php the_sub_field('news_url') ?>" target="_blank" class="btn -blue">Read More</a>
      <?php } ?>

      </p>
    <?php endwhile; ?>



  </div>

</div>
