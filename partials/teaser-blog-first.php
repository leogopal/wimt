<article id="post-<?php the_ID(); ?>" <?php post_class('c-teaser -first'); ?>>

  <figure class="c-teaser-img">

    <a href="<?php the_permalink() ?>">

    <img src="<?php the_field('post_image'); ?>" alt="<?php the_title(); ?>" />

    </a>

  </figure>

  <div class="c-teaser-content">

    <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

    <?php the_excerpt(); ?>

    <a href="<?php the_permalink() ?>" class="btn -yellow">Read More</a>

  </div>

</article>
