<div class="section form m-signup -<?php the_sub_field('block_mailing_list') ?>">

  <div class="container">

    <header class="form-header">
      <p><?php the_field('weekly_list', 'option'); ?></p>
    </header>

    <div class="m-signup-form">
	    <?php gravity_form( 6, false, false, false, '', true, 12 ); ?>
    </div>

  </div>

</div>
