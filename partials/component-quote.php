<aside class="section -large c-quote">

  <?php the_sub_field('quote_text') ?>

  <a href="<?php the_sub_field('button_url') ?>" class="btn -yellow -inline"><?php the_sub_field('button_text') ?></a>

</aside>
