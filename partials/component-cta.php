<aside class="section -large c-cta">

  <div class="container">

    <header class="c-cta-header">

      <h2><?php the_sub_field('title') ?></h2>
      <p><?php the_sub_field('text') ?></p>

    </header>

    <a href="<?php the_sub_field('button_url') ?>" class="btn -blue -inline  reveal"><?php the_sub_field('button_text') ?></a>

    <ul>
      <li>Phone Us
        <a href="tel:<?php the_field('global_phone', 'option'); ?>">
          <?php the_field('global_phone', 'option'); ?>
        </a>
      </li>
      <li>Email Us
        <a href="mailto:<?php the_field('global_email', 'option'); ?>">
          <?php the_field('global_email', 'option'); ?>
        </a>
      </li>
    </ul>

  </div>

</aside>
