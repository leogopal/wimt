<aside class="modal" id="modal-contact">

    <div class="modal-body">

        <a href="#modal-contact" class="reveal modal-closer">

        </a>

        <h2>Arrange a meeting</h2>
        <p>Leave your details and we will get back to you to arrange a meeting or call.</p>

		<?php gravity_form( 1, false, false, false, '', true, 12 ); ?>

    </div>

</aside>


