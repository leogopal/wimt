<div class="section c-founders">

  <div class="container">

    <h3>Our Founders</h3>

    <ul>

      <?php while ( have_rows('founder') ) : the_row(); ?>
      <li>

        <img src="<?php the_sub_field('photo') ?>" alt="" class="" />

        <h4><?php the_sub_field('name') ?></h4>
        <em><?php the_sub_field('designation') ?></em>
        <a href="<?php the_sub_field('linkedin') ?>">Find on LinkedIn</a>


      </li>

      <?php endwhile; ?>

    </ul>

  </div>

</div>
