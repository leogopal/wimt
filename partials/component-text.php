<div class="section c-text  m-layout--<?php the_sub_field('layout') ?>">

  <div class="container">

    <?php if ( get_sub_field('title') ) { ?>
    <p class="lead"><?php the_sub_field('title') ?></p>
    <?php  }; ?>
    <?php if ( get_sub_field('text') ) { ?>
    <p><?php the_sub_field('text') ?></p>
    <?php  }; ?>

  </div>

</div>
