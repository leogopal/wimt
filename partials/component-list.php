<div class="c-list section m-layout--<?php the_sub_field('layout') ?> -<?php the_sub_field('type') ?>">

  <div class="container">

  <?php if ( get_sub_field('list_title') || get_sub_field('list_text') ) { ?>

    <header class="c-list-header">

      <?php if ( get_sub_field('list_title') ) { ?>
        <h3><?php the_sub_field('list_title') ?></h3>
      <?php  }; ?>
      <?php if ( get_sub_field('list_text') ) { ?>
        <p><?php the_sub_field('list_text') ?></p>
      <?php  }; ?>

    </header>

    <?php  }; ?>

    <ul class="grid -gutter">

    <?php while ( have_rows('list') ) : the_row(); ?>

      <li class="grid-item">

        <?php if( get_sub_field('list_icon')): ?>
          <figure class="c-list-img">
            <img src="<?php the_sub_field('list_icon') ?>" />
          </figure>
        <?php endif; ?>

        <div class="c-list-content">

          <?php if( get_sub_field('list_title')): ?>
            <h4><?php the_sub_field('list_title') ?></h4>
          <?php endif; ?>

          <?php if( get_sub_field('list_text')): ?>
            <p><?php the_sub_field('list_text') ?></p>
          <?php endif; ?>

        </div>

      </li>

    <?php endwhile; ?>

    </ul>

  </div>

</div>
