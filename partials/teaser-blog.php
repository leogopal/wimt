<article id="post-<?php the_ID(); ?>" <?php post_class('grid-item c-teaser teaser-blog'); ?>>

  <figure class="c-teaser-img">
    <a href="<?php the_permalink() ?>">
      <img src="<?php the_field('post_image'); ?>" alt="<?php the_title(); ?>" />
    </a>
  </figure>

  <?php the_title( sprintf( '<h4 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>

  <footer class="c-teaser-footer">
    <ul>
      <li><?php the_time(get_option('date_format')); ?></li>
      <li>|&nbsp;&nbsp;&nbsp;by <?php 

            if ( function_exists( 'coauthors' ) ) {
              coauthors();
          } else {
              the_author();
          }
          
          ?></li>
    <ul>

  </footer><!-- .entry-footer -->
</article>
