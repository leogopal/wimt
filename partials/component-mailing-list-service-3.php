<div class="section form m-signup -quarterly">

  <div class="container">

    <header class="form-header">
      <p>Sign up to our quarterly Implementation Partners newsletter for product developments and industry news.</p>
    </header>

    <div class="m-signup-form">
	    <?php gravity_form( 2, false, false, false, '', true, 12 ); ?>
    </div>

  </div>

</div>
