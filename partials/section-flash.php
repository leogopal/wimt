<?php if (get_field('flash_active', 'option') == 1) { ?>
<aside class="c-flash">

  <ul>
    <li><span><?php the_field('flash_date', 'option'); ?></span></li>
    <li><?php the_field('flash_message', 'option'); ?></li>
    <li><a href="<?php the_field('flash_url', 'option'); ?>">MORE</a></li>
  </ul>

</aside>
<?php }; ?>
