<article id="post-<?php the_ID(); ?>" <?php post_class('grid-item c-teaser m-teaser-news'); ?>>

  <div class="c-teaser-content">

    <?php if ( get_field('add_media') == 'external' ) { ?>

      <?php the_field('media'); ?>

    <?php } ?>

    <?php if ( get_field('add_media') == 'image' ) { ?>

      <img src="<?php the_field('image'); ?>" alt="<?php the_title(); ?>" />

    <?php } ?>

    <?php if ( get_field('add_media') == 'none' ) { ?>
      <h5><?php the_time(get_option('date_format')); ?></h5>
      <h2><?php the_title(); ?></h2>
    <?php } ?>

  </div>

  <div class="c-teaser-content">

    <?php if ( get_field('add_media') != 'none' ) { ?>
      <h5><?php the_time(get_option('date_format')); ?></h5>
      <h3><?php the_title(); ?></h3>
    <?php } ?>

    <?php the_field('text'); ?>

  </div>

</article>
