<div class="m-contact">

  <?php while ( have_rows('office') ) : the_row(); ?>

  <div class="grid">

    <div class="m-contact-details section">

      <h2><?php the_sub_field('city') ?></h2>

      <ul>
        <li>
          <small>Find us</small>
          <?php the_sub_field('address') ?>
        </li>
        <?php if (get_sub_field('phone')) { ?>
        <li>
          <small>Phone us</small>
          <?php the_sub_field('phone') ?>
        </li>
        <?php } ?>
        <li>
          <small>Email us</small>
          <?php the_sub_field('email') ?>
        </li>
      </ul>

    </div>

    <figure class="m-contact-map">

      <?php
        $values = get_sub_field('location');
        $lat = $values['lat'];
        $lng = $values['lng'];
        $address = $values['address'];
        $map_center_lat = $values['center_lat'];
        $map_center_lng = $values['center_lng'];
        $map_zoom = $values['zoom'];
      ?>

      <?php

      if( !empty($values) ):
      ?>
      <div class="acf-map">
      	<div class="marker" data-lat="<?php echo $values['lat']; ?>" data-lng="<?php echo $values['lng']; ?>"></div>
      </div>

    <?php endif; ?>

    </figure>

  </div>

  <?php endwhile; ?>

</div>
