<div class="c-list section <?php the_sub_field('custom_class') ?>">



  <?php if ( get_sub_field('list_title') ) { ?>
    <h3><?php the_sub_field('list_title') ?></h3>
  <?php  }; ?>
  <?php if ( get_sub_field('list_text') ) { ?>
    <p><?php the_sub_field('list_text') ?></p>
  <?php  }; ?>



  <?php while ( have_rows('list') ) : the_row(); ?>

    <div class="slide">

      <?php if( get_sub_field('list_icon')): ?>
        <figure class="c-list-img">
          <img src="<?php the_sub_field('list_icon') ?>" />
        </figure>
      <?php endif; ?>

      <div class="c-list-content">

        <?php if( get_sub_field('list_title')): ?>
          <p class="lead"><?php the_sub_field('list_title') ?></p>
        <?php endif; ?>

        <?php if( get_sub_field('list_text')): ?>
          <p><?php the_sub_field('list_text') ?></p>
        <?php endif; ?>

      </div>

    </div>

  <?php endwhile; ?>

</div>
