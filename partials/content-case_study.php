<article id="post-<?php the_ID(); ?>" <?php post_class('section c-content'); ?>>

  <div class="container">

    <header class="c-content-header">

      <?php
      $clients = wp_get_post_terms( $post->ID, 'case_study_client', array("fields" => "names") );
      $cat = wp_get_post_terms( $post->ID, 'case_study_category', array("fields" => "names") );

   ?>
      <ul>
        <li class="blue"><em><?php echo implode(', ',$clients); ?></em></li>
      </ul>

      <h1><?php the_title(); ?></h1>

    </header>

    <figure class="c-content-img">
      <img src="<?php the_field('post_image'); ?>" alt="<?php the_title(); ?>" />
    </figure>

    <div class="c-content-body">

      <?php the_content(); ?>

    </div>

    <footer class="c-content-footer">
      <ul>
        <li>
          <?php do_action( 'addthis_widget', get_permalink(), get_the_title(), 'large_toolbox'); ?>
        </li>
        <li><?php the_tags(); ?></li>
      <ul>

    </footer><!-- .entry-footer -->
    <p>&nbsp;</p>
  </div>

</article>
