<p><strong>CASE STUDY</strong></p>

<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

<?php the_excerpt(); ?>

<a href="<?php the_permalink() ?>" class="btn -yellow">Read Article</a>
