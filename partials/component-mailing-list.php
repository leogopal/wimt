<?php 

if (get_sub_field('block_mailing_list') == 'quarterly') {

  get_template_part('partials/component', 'mailing-list-quarterly');
  // echo the_sub_field('block_mailing_list');

};

if (get_sub_field('block_mailing_list') == 'service_1') {

  get_template_part('partials/component', 'mailing-list-service-1');
  // echo the_sub_field('block_mailing_list');

};

if (get_sub_field('block_mailing_list') == 'service_2') {

  get_template_part('partials/component', 'mailing-list-service-2');
  // echo the_sub_field('block_mailing_list');

};

if (get_sub_field('block_mailing_list') == 'service_3') {

  get_template_part('partials/component', 'mailing-list-service-3');
  // echo the_sub_field('block_mailing_list');

}

if (get_sub_field('block_mailing_list') == 'weekly') {

  get_template_part('partials/component', 'mailing-list-weekly');
  // echo the_sub_field('block_mailing_list');

} ?>
