<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="mast">

      <!-- 2.1. BLOG INTRODUCTION - - - - - - - - - - - - - - - - - - - - -  -->

      <?php

      $args = array (
        'post_type'              => 'any',
        'p' => '267'
      );

      $the_query = new WP_Query( $args );

      ?>

      <!-- 2.2.2.1. End -->

      <!-- 2.2.2.2. The loop -->
      <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php

        // are there any rows within within our flexible content?
        if( have_rows('wimt_content') ):

          // loop through all the rows of flexible content
          while ( have_rows('wimt_content') ) : the_row();

          // PAGE HEADER
          if( get_row_layout() == 'block_header' )
            get_template_part('partials/component', 'header');

          endwhile; // close the loop of flexible content
        endif; // close flexible content conditional

        ?>

      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>

      <?php else:  ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>

      <!-- 2.1. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <aside class="cs-back">

        <div class="container">

          <a href="<?php echo site_url() ?>/case-studies">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/icon--arrow-left.svg" class="c-icon " />
            Case Studies
          </a>

        </div>

      </aside>

      <!-- 2.2. LATEST POST - - - - - - - - - - - - - - - - - - - - - - - -  -->

      <?php
  		// Start the loop.
  		while ( have_posts() ) : the_post();

  			// Include the page content template.
  			get_template_part( 'partials/content', 'case_study' );

  			// If comments are open or we have at least one comment, load up the comment template.
  			if ( comments_open() || get_comments_number() ) {
  				//comments_template();
  			}

  			// End of the loop.
  		endwhile;
  		?>
      <!-- 2.2. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.4. BLOG LISTINGS - - - - - - - - - - - - - - - - - - - - - - -  -->

      <section class="section m-listings m-layout--grayer">

        <div class="container">

        <!-- 2.4.1. BLOG LISTINGS HEADER -->

        <header class="m-listings-header">

          <h3>Recent Articles</h3>

        </header>

        <!-- 2.4.1. END -->

        <!-- 2.4.2. BLOG LISTINGS HEADER -->

        <div class="grid -gutter">

          <?php

          $post_type = get_field( 'case_study' );

          ?>

          <!-- 2.2.2. THE LOOP -->

            <!-- 2.2.2.1. Initialise the query -->

            <?php

            $args = array (
              'post_type'              => 'case_study',
              'posts_per_page'         => '2',
              'orderby'                => 'rand',
              'ignore_sticky_posts'    => false,
              'order'                  => 'DESC'
            );

            $the_query = new WP_Query( $args );

            ?>

            <!-- 2.2.2.1. End -->

            <!-- 2.2.2.2. The loop -->
            <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

              <?php get_template_part('partials/teaser', 'case_study'); ?>

            <?php endwhile; ?>

            <?php wp_reset_postdata(); ?>

            <?php else:  ?>
              <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
            <?php endif; ?>
            <!-- 2.2.2.2. End -->

        </div>

        <!-- 2.4.2. END -->

      </div>

      </section>

      <!-- 2.4. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <?php

      $args = array (
        'post_type'              => 'any',
        'p' => '267'
      );

      $the_query = new WP_Query( $args );

      ?>

      <!-- 2.2.2.1. End -->

      <!-- 2.2.2.2. The loop -->
      <?php if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

        <?php

        // are there any rows within within our flexible content?
        if( have_rows('wimt_content') ):

          // loop through all the rows of flexible content
          while ( have_rows('wimt_content') ) : the_row();

          // PAGE HEADER
          // MAILING LIST
          if( get_row_layout() == 'block_mailing_list' )

            get_template_part('partials/component', 'mailing-list');

          // MAILING LIST
          if( get_row_layout() == 'block_linked')

            get_template_part('partials/component', 'linked');

          endwhile; // close the loop of flexible content
        endif; // close flexible content conditional

        ?>

      <?php endwhile; ?>

      <?php wp_reset_postdata(); ?>

      <?php else:  ?>
        <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
      <?php endif; ?>


    </section>

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
