<?php get_header(); ?>

    <!-- 2. CONTENT ++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

    <section class="mast">

      <!-- 2.1. BLOG INTRODUCTION - - - - - - - - - - - - - - - - - - - - -  -->

      <header class="section blog-header">

        <div class="container">

          <a href="<?php site_url(); ?>/blog">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/logo--interchange.svg" class="blog-header-logo" alt="Where is My Transport" />
          </a>

          <h5>By <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/site-logo-gray.svg" alt="Where is My Transport" /></h5>

        </div>

      </header>

      <!-- 2.1. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <aside class="b-back">

        <div class="container">

          <a href="<?php echo site_url() ?>/blog">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/site/icon--arrow-left.svg" class="c-icon " />
          </a>

        </div>

      </aside>

      <!-- 2.2. LATEST POST - - - - - - - - - - - - - - - - - - - - - - - -  -->

      <?php
  		// Start the loop.
  		while ( have_posts() ) : the_post();

  			// Include the page content template.
  			get_template_part( 'partials/content', 'blog' );

  			// If comments are open or we have at least one comment, load up the comment template.
  			if ( comments_open() || get_comments_number() ) { ?>

          <div class="section -special">

            <div class="container">

              <a href="#toggle-comments" class="reveal dissapear -commentbtn btn -black">Load Comments</a>

              <aside id="toggle-comments" class="section -commentary">

                <?php comments_template(); ?>

              </aside>

            </div>

        </div>

  		<?php	}

  			// End of the loop.
  		endwhile;
  		?>
      <!-- 2.2. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.2. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->
      <!-- 2.2. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.3. SIGNUP BLOCK  - - - - - - - - - - - - - - - - - - - - - - -  -->

      <?php get_template_part('partials/component', 'mailing-list'); ?>

      <!-- 2.3. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->

      <!-- 2.4. BLOG LISTINGS - - - - - - - - - - - - - - - - - - - - - - -  -->

      <section class="section m-listings m-layout--grayer">

        <div class="container">

          <!-- 2.4.1. BLOG LISTINGS HEADER -->

          <header class="m-listings-header">

            <h3>Suggested articles</h3>

          </header>

          <!-- 2.4.1. END -->

          <!-- 2.4.2. BLOG LISTINGS HEADER -->

          <div class="grid -gutter">

            <?php
            $orig_post = $post;
            global $post;
            $tags = wp_get_post_tags($post->ID);

            if ($tags) {
            $tag_ids = array();
            foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
            $args=array(
            'tag__in' => $tag_ids,
            'post__not_in' => array($post->ID),
            'posts_per_page'=>2, // Number of related posts to display.
            'caller_get_posts'=>1
            );

            $my_query = new wp_query( $args );

            while( $my_query->have_posts() ) {
            $my_query->the_post();

            get_template_part('partials/teaser', 'blog');

             }
            }
            $post = $orig_post;
            wp_reset_query();
            ?>

          </div>

          <!-- 2.4.2. END -->

        </div>

      </section>

      <!-- 2.4. END  - - - - - -  - - - - - - - - - - - - - - - - - - - - -  -->


    </section>

    <!-- 2. END ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->

<?php get_footer(); ?>
